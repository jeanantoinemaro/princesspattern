using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Assets.View.Common.Scripts.Controller.Characters.Ennemy;
using Assets.View.Common.Scripts.Controller.Characters.Players;
using Assets.View.Common.Scripts.Model.Util;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace Assets.View.Common.Scripts.Model.State
{
    public class WonState:State
    {
        public override IEnumerator Start()
        {

            int xpValue = 0;
            foreach (EnnemyController ec in BattleEngine.Instance.GetEnnemyControllers())
            {
                xpValue += (ec.GetFighter().getLevel() * 10);
            }
            int playerCharacterNumber = BattleEngine.Instance.GetPlayerControllers().Count;
            int xpGain = xpValue / playerCharacterNumber;

            string xpMessage = "Victory\nYou win " + xpGain + "xp.";
            string levelUpMessage = null;
            foreach (PlayerController pc in BattleEngine.Instance.GetPlayerControllers())
            {
                levelUpMessage = pc.GetPlayer().AddExperiences(xpGain);
            }
            if (levelUpMessage != null)
            {
                xpMessage += "\n" + levelUpMessage;
            }
            BattleEngine.Instance.BattleUI.SetDialogText(xpMessage);

            yield return new WaitForSeconds(5f);
            SaveFighters.Save();
            BattleEngine.Instance.GetPlayerControllers().Clear();
            int currentEnnemyIndex = WorldEngine.Instance.GetEnnemiesPrefabsClone()
                .IndexOf(WorldEngine.Instance.GetCurrentEnnemy());
            GameEngine.Instance.GetEnnemiesPrefabs()[currentEnnemyIndex].gameObject.SetActive(false);
            GameEngine.Instance.AddKey();
            SceneManager.LoadScene(BattleEngine.Instance.GetAssociatedWorldScene());
            yield break;
        }
    }
}

using System;
using Assets.View.Common.Scripts.Model.Map;
using System.Collections.Generic;
using Assets.View.Common.Scripts.Controller.Characters;
using Assets.View.Common.Scripts.Controller.Characters.Ennemy;
using Assets.View.Common.Scripts.Controller.Characters.Players;
using Assets.View.Common.Scripts.Enum;
using Assets.View.Common.Scripts.Model.Fighter.Ennemy;
using Assets.View.Common.Scripts.Template.Character;
using JetBrains.Annotations;
using NUnit.Framework.Constraints;
using UnityEngine;
using UnityEngine.Tilemaps;

namespace Assets.View.Common.Scripts
{
    public class MapEngine : MonoBehaviour
    {
        public static MapEngine Instance;

        public GameObject overlayPrefab;
        public GameObject overlayContainer;

        [SerializeField] private int _width;
        [SerializeField] private int _height;
        [SerializeField] private int _depth;
        [SerializeField] private Tilemap tileMap;

        public Dictionary<Vector2Int, TileGo> map;
        public bool ignoreBottomTiles;

        private void Awake()
        {
            if (Instance != null && Instance != this)
            {
                Destroy(this.gameObject);
            }
            else
            {
                Instance = this;
            }
        }

        void Start()
        {
            SetupTileMap();
            InstantiateEnemies();
        }

        private void InstantiateEnemies()
        {
            List<GameObject> _ennemies = BattleEngine.Instance.GetEnnemmyPrefabs();
            int i = _ennemies.Count - 1;
            System.Random rnd = new System.Random();
            while (i >= 0)
            {
                int x = rnd.Next(_width - 3, _width);  // creates a number between x max-3 et x max (3derniers rangs)
                int y = rnd.Next(0, _height);
                int z = rnd.Next(0, _depth);
                TileGo spawnTile = GetTileAtPosition(new Vector3Int(x, y, z));
                if (this.tileMap.HasTile(new Vector3Int(x, y, z)) && spawnTile && !spawnTile.isBlocked)
                {

                    GameObject ennemyGo = Instantiate(_ennemies[i], BattleEngine.Instance.transform);

                    FighterTemplate template = ennemyGo.GetComponent<FighterTemplate>();
                    string characterSosName =
                        System.Enum.GetName(typeof(PlayerCharacterEnum),
                        template.GetPlayerCharacterEnum());
                    Ennemy enemy = new Ennemy(
                        template.GetName(),
                        GameEngine.Instance.GetCharacter(characterSosName));

                    EnnemyController ec = BattleEngine.Instance.AddEnnemyController(enemy, template);

                    ennemyGo.transform.position = spawnTile.transform.position;
                    ec.SetStandingOnTile(spawnTile);
                    ennemyGo.GetComponent<SpriteRenderer>().sortingOrder = spawnTile.GetComponent<SpriteRenderer>().sortingOrder;
                    i--;
                }
            }
        }

        private void SetupTileMap()
        {
            map = new Dictionary<Vector2Int, TileGo>();
            var tileMap = GetComponentInChildren<Tilemap>();
            for (int z = _depth; z >= 0; z--)
            {
                for (int x = 0; x < _width; x++)
                {
                    for (int y = 0; y < _height; y++)
                    {
                        var tileLocation = new Vector3Int(x, y, z);
                        var key = new Vector2Int(x, y);
                        if (tileMap.HasTile(tileLocation) && !map.ContainsKey(key))
                        {
                            GameObject tile = Instantiate(overlayPrefab, overlayContainer.transform);
                            tile.transform.position = tileMap.GetCellCenterWorld(new Vector3Int(x, y, z)) + new Vector3Int(0, 0, 1);
                            tile.GetComponent<SpriteRenderer>().sortingOrder =
                                tileMap.GetComponent<TilemapRenderer>().sortingOrder;
                            tile.GetComponent<TileGo>().gridLocation = new Vector3Int(x, y, z);
                            map.Add(key, tile.GetComponent<TileGo>());

                            if (x < 3 && !tile.GetComponent<TileGo>().isBlocked)
                            {
                                tile.GetComponent<TileGo>().Highlight2();
                            }
                        }
                    }
                }
            }
        }

        public TileGo GetTileAtPosition(Vector3Int position)
        {
            if (map.ContainsKey(new Vector2Int(position.x, position.y)))
            {
                return map[new Vector2Int(position.x, position.y)];
            }
            return null;
        }

        public void SetTilesTo(bool value, int range)
        {
            PlayerController playerController = (PlayerController) BattleEngine.Instance.GetCurrentPlayerController();
            if (playerController == null)
            {
                return;
            }
            var playerTile = playerController.GetStandingOnTile();
            var tileMap = GetTileMap();
            var map = GetMap();
            //var cellCoordinate = tileMap.WorldToCell(playerTile.transform.position);
            for (int z = MapEngine.Instance._depth; z >= 0; z--)
            {
                for (int x = -range; x <= range; x++)
                {
                    for (int y = -range; y <= range; y++)
                    {
                        if (Math.Abs(x)+ Math.Abs(y) > range )
                        {
                            continue;
                        }
                        var tileLocation = tileMap.WorldToCell(playerTile.transform.position) + new Vector3Int(x, y, z);
                        if (!map.ContainsKey(new Vector2Int(tileLocation.x, tileLocation.y)))
                        {
                            continue;
                        }
                        TileGo tile = GetTileAtPosition(tileLocation);
                        
                        if (value)
                        {
                            tile.Highlight2();

                        } else
                        {
                            tile.DeactivateHighlight2();
                        }
                    }
                }
            }
        }

        #region GETTERS

        public Dictionary<Vector2Int, TileGo> GetMap()
        {
            return map;
        }
        public Tilemap GetTileMap()
        {
            return tileMap;
        }

        public Grid GetGrid()
        {
            return GetComponent<Grid>();
        }

        public int GetWidth()
        {
            return _width;
        }

        public int GetHeight()
        {
            return _height;
        }

        public int GetDepth()
        {
            return _depth;
        }
        #endregion
    }
}

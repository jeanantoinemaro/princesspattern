namespace Assets.View.Items.Scripts.Enums
{
    public enum EStats
    {
        Life,
        Attack,
        Defense,
        Initiative,
        AP,
        MP
    }
}


﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Assets.View.Common.Scripts.Controller.Characters;
using UnityEngine;
using UnityEngine.PlayerLoop;

namespace Assets.View.Common.Scripts.Template.Character
{
    public class SelectCharacter: MonoBehaviour
    {
        #region Unity Methods

        public void OnMouseDown()
        {
            gameObject.GetComponent<FighterTemplate>().OnSelected();
            //StartCoroutine(BattleEngine.Instance.GetState()
            //    .SelectPlayerController(BattleEngine.Instance.GetCurrentPlayerController()));
           StartCoroutine(BattleEngine.Instance.GetState().TargetChoice());
        }

        public void OnMouseOver()
        {
            GetComponent<SpriteRenderer>().color = new Color(.5f, .5f, .5f);
            //Color color;
            //if (ColorUtility.TryParseHtmlString("#E2270A", out color))
            //{
            //    GetComponent<SpriteRenderer>().color = color;
            //}
            //GetComponent<SpriteRenderer>().color = Color;
        }

        public void OnMouseExit()
        {
            GetComponent<SpriteRenderer>().color = Color.white;
        }

        public void Update()
        {
            if (Input.GetKeyDown(KeyCode.Escape))
            {
                
                StartCoroutine(BattleEngine.Instance.GetState().CancelAttack());
            }
        }
        #endregion
    }
}

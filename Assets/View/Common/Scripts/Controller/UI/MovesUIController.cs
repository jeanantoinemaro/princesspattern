﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mime;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;
using UnityEngine.UI;
using BattleAction = Assets.View.Common.Scripts.ScriptableObjects.BattleAction;

namespace Assets.View.Common.Scripts.Controller.UI
{
    public class MovesUIController:MonoBehaviour
    {
        #region Serialized Field

        [SerializeField] private Transform _movesContainer;
        [SerializeField] private GameObject _movePrefab;
        [SerializeField] private List<BattleAction> _moves;

        #endregion

        #region Unity Methods


        void OnEnable()
        {
            foreach (Transform moveUI in _movesContainer)
            {
                Destroy(moveUI.gameObject);
            }
            _moves = BattleEngine.Instance.GetCurrentPlayerController().GetFighter().GetCharacter().GetActions();

            int actionPointsLeft = BattleEngine.Instance.GetCurrentPlayerController().GetFighter().GetCharacter().GetActionPoint();

            foreach (BattleAction move in _moves)
            {
                var moveUI = Instantiate(_movePrefab, _movesContainer);
                var moveUIName = moveUI.transform.GetComponent<Text>();
                moveUIName.text = move.name + " (range: " + move.GetRange() + ")";

                if (move.GetActionCost() <= actionPointsLeft)
                {
                    moveUI.GetComponent<MoveUIController>().SetMove(move);
                } else
                {
                    moveUIName.color = Color.gray;
                }
                
                
            }
        }
        #endregion
    }
}

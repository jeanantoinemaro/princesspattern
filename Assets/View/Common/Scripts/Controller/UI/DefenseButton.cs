using System.Collections;
using System.Collections.Generic;
using Assets.View.Common.Scripts.Model.State;
using UnityEngine;

namespace Assets.View.Common.Scripts.Controller.UI
{
    public class DefenseButton : MonoBehaviour
    {
        public void Defend()
        {
            BattleEngine.Instance.GetCurrentPlayerController().Defend();
            BattleEngine.Instance.GetCurrentPlayerController().GetFighterTemplate().PlayAnimator("DEFENDING");
            BattleEngine.Instance.GetCurrentPlayerController().SetIsReady(false);
            BattleEngine.Instance.GetActions().gameObject.SetActive(false);
            BattleEngine.Instance.SetState(new SelectNextPlayer());
        }
    }
}
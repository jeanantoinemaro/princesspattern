﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Assets.View.Common.Scripts.Model.Map;
using Assets.View.Common.Scripts.ScriptableObjects;
using Assets.View.Common.Scripts.ScriptableObjects.AdvancedCharacter;
using Assets.View.Items.Scripts;
using UnityEngine;

namespace Assets.View.Common.Scripts.Model.Fighter
{
    [Serializable]
    public class Fighter
    {

        #region Private fields
        protected int _level;
        private int _id;
        private string _name;
        private AdvancedCharacter _character;
        private TileGo _standingOnTile;
        private TileGo _destinationTile;
        //private bool _selected;
        protected bool _isDefending;
        public event Action<string> OnNameChanged;
        #endregion

        public Fighter(string name, AdvancedCharacter character)
        {
            _name = name;
            _character = character;
            _level = 1;
        }

        #region Public methods

        public void SetName(string pName)
        {
            if (_name != pName)
            {
                _name = pName;
                OnNameChanged?.Invoke(pName);
            }
        }
        public override string ToString()
        {
            return _name;
        }
        #endregion

        #region Getters Setters
        public TileGo GetStandingOnTile()
        {
            return _standingOnTile;
        }

        public void SetStandingOnTile(TileGo tile)
        {
            if (_standingOnTile)
            {
            _standingOnTile.isBlocked = false;
            }
            _standingOnTile = tile;
            tile.isBlocked = true;
        }

        public void SetIsDefending(bool pBool)
        {
            _isDefending = pBool;
        }
        public bool IsDefending()
        {
            return _isDefending;
        }

        public int getLevel()
        {
            return _level;
        }


        //public bool GetSelected()
        //{
        //    return _selected;
        //}

        //public void SetSelected(bool pSelected)
        //{
        //    _selected = pSelected;
        //}



        public AdvancedCharacter GetCharacter()
        {
            return _character;
        }

        public void SetDestinationTile(TileGo tile)
        {
            _destinationTile = tile;
        }

        public TileGo GetDestinationTile()
        {
            return _destinationTile;
        }

        public string GetName()
        {
            return _name;
        }

        #endregion
    }
}

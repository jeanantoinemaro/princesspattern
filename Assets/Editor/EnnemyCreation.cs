using System.Collections;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using Assets.View.Common.Scripts.Enum;
using Assets.View.Common.Scripts.ScriptableObjects;
using Assets.View.Common.Scripts.Template.Character;
using Assets.View.Common.Scripts.Template.Ennemy;
using UnityEditor;
using UnityEditor.Animations;
using UnityEngine;
using UnityEngine.VFX;

public class EnnemyCreation : EditorWindow
{
    private string _characterName = "";
    private PlayerCharacterEnum _characterClass;
    private Sprite _characterSprite;
    private AnimatorController _characterAnimator;

        [MenuItem("Tools/Ennemy Creation")]
    public static void ShowWindow()
    {
        GetWindow(typeof(EnnemyCreation));
    }

    private void OnGUI()
    {
        GUILayout.Label("New ennemy", EditorStyles.boldLabel);

        _characterName = EditorGUILayout.TextField("Name", _characterName);
        _characterClass = (PlayerCharacterEnum) EditorGUILayout.EnumFlagsField("Class", _characterClass, GUIStyle.none);
        _characterSprite = (Sprite) EditorGUILayout.ObjectField("Sprite", _characterSprite, typeof(Sprite), true);
        _characterAnimator = (AnimatorController) EditorGUILayout.ObjectField("Animator", _characterAnimator, typeof(AnimatorController), false);

        if (GUILayout.Button("Create Ennemy"))
        {
            Create();
        }

    }
    private void Create()
    {
        if (_characterClass == 0)
        {
            Debug.Log("Please choose a class");
            return;
        }

        if (_characterName == "")
        {
            Debug.Log("Please enter a name");
            return;
        }

        Object source = Resources.Load("EnnemyPrefab");

        GameObject Go = (GameObject)PrefabUtility.InstantiatePrefab(source);
        Go.GetComponent<FighterTemplate>().SetName(_characterName);
        Go.GetComponent<FighterTemplate>().SetPlayerCharacterEnum(_characterClass);
        Go.GetComponent<FighterTemplate>().SetSprite(_characterSprite);
        Go.AddComponent<Animator>();
        Go.GetComponent<Animator>().runtimeAnimatorController = _characterAnimator;
        Go.transform.localScale = new Vector3(0.18f, 0.18f, 1);
        
        PrefabUtility.SaveAsPrefabAsset(Go, "Assets/View/Common/Prefabs/AdvancedCharacters/Enemies/"+_characterName+".prefab");
        DestroyImmediate(Go);
    }

}
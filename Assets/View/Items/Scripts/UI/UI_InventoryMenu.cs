using System;
using System.Collections.Generic;
using UnityEngine;

namespace Assets.View.Items.Scripts.UI
{
    public class UI_InventoryMenu : MonoBehaviour
    {
        #region Serialized Fields

        [SerializeField]
        private UI_InventoryItem _itemPrefab;
        [SerializeField]
        private RectTransform _contentPanel;

        #endregion

        #region Fields

        private List<UI_InventoryItem> _uiItems = new List<UI_InventoryItem>();

        public event Action<int> OnDetailsRequested;
        public event Action<int> OnItemActionRequested;

        #endregion

        #region Public Methods

        public void InitializeInventoryUI(int pInventorySize)
        {
            for (int i = 0; i < pInventorySize; i++)
            {
                UI_InventoryItem uiItem = Instantiate(_itemPrefab, Vector3.zero, Quaternion.identity);
                uiItem.transform.SetParent(_contentPanel);
                _uiItems.Add(uiItem);
                uiItem.OnItemClicked += HandleItemSelection;
                uiItem.OnItemRightClicked += HandleUseItem;
            }
        }

        public void UpdateData(int pItemIndex, Sprite pItemImage, int pItemQuantity)
        {
            if (_uiItems.Count > pItemIndex)
            {
                _uiItems[pItemIndex].SetData(pItemImage, pItemQuantity);
            }
        }

        public void UpdateDetails(int itemIndex, Sprite sprite, string name, object p)
        {
            // TODO set details
            DeselectAllItems();
            _uiItems[itemIndex].Select();
        }

        public void Show()
        {
            gameObject.SetActive(true);
            ResetSelection();
        }

        public void ResetSelection()
        {
            // reset item details
            DeselectAllItems();
        }

        public void ResetAllItems()
        {
            foreach (var item in _uiItems)
            {
                item.ResetData();
                item.Deselect();
            }
        }

        public void Hide()
        {
            gameObject.SetActive(false);
        }

        #endregion

        #region Private Methods

        private void HandleItemSelection(UI_InventoryItem pUiInventoryItem)
        {
            int index = _uiItems.IndexOf(pUiInventoryItem);
            if (index == -1)
                return;
            _uiItems[index].Select();
            // invoke action to display item details
        }

        private void HandleUseItem(UI_InventoryItem pUiInventoryItem)
        {
            int index = _uiItems.IndexOf(pUiInventoryItem);
            if (index == -1)
                return;
            OnItemActionRequested?.Invoke(index);
        }

        private void DeselectAllItems()
        {
            foreach (UI_InventoryItem item in _uiItems)
            {
                item.Deselect();
            }
        }

        #endregion
    }
}
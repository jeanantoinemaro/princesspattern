using Assets.View.Common.Scripts.Model.Util;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace Assets.View.Common.Scripts.Controller.UI
{
    public class RetryGameButton : MonoBehaviour
    {
        public void LoadCharacterSelectionScene()
        {
            SceneManager.LoadScene("World");
        }
    }
}

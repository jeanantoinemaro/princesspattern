using System.Collections.Generic;
using Assets.View.Common.Scripts.Template.Character;
using UnityEngine;

namespace Assets.View.Common.Scripts.Controller.UI
{
    public class DispatchMenuController : MonoBehaviour
    {

        #region Serialized fields

        [SerializeField] private List<GameObject> _playersGo;
        [SerializeField] private UICharacter _uiCharacter;
        [SerializeField] private List<UICharacter> _uiCharacters;

        #endregion

        #region Unity Methods
        // Start is called before the first frame update
        void Awake()
        {
            _playersGo = GameEngine.Instance.GetPlayerBattlePrefabs();
        }

        public void Start()
        {
            foreach (GameObject playerPrefab in _playersGo)
            {
                UICharacter uic = Instantiate(_uiCharacter, transform);

                uic.SetPlayerPrefab(playerPrefab);
                uic.SetSelected(false);
                //playerPrefab.GetComponent<FighterTemplate>().SetSelectedForUI(false);
                uic.SetSprite(playerPrefab.GetComponent<FighterTemplate>().GetSprite().sprite);
                uic.SetName(playerPrefab.name);
                _uiCharacters.Add(uic);
                //uic.gameObject.SetActive(false);
            }
        }
        #endregion

        #region Getters Setters

        public List<UICharacter> GetUiCharacters()
        {
            return _uiCharacters;
        }
        #endregion
    }
}
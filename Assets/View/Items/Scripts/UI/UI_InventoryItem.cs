using System;
using TMPro;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

namespace Assets.View.Items.Scripts.UI
{
    public class UI_InventoryItem : MonoBehaviour
    {
        #region Serialized Fields

        [SerializeField] private Image _itemImage;
        [SerializeField] private TMP_Text _quantityTxt;
        [SerializeField] private Image _borderImage;

        #endregion

        #region Fields

        public event Action<UI_InventoryItem> OnItemClicked, OnItemRightClicked; //, OnItemDroppedOn, OnItemBeginDrag, OnItemEndDrag;
        private bool _empty = true;

        #endregion

        #region Unity Methods

        public void Awake()
        {
            ResetData();
            Deselect();
        }

        #endregion

        #region Public Methods

        public void ResetData()
        {
            _itemImage.gameObject.SetActive(false);
            _empty = true;
        }

        public void Deselect()
        {
            _borderImage.enabled = false;
        }

        public void SetData(Sprite pSprite, int pQuantity)
        {
            _itemImage.gameObject.SetActive(true);
            _itemImage.sprite = pSprite;
            _quantityTxt.text = pQuantity + "";
            _empty = false;
        }

        public void Select()
        {
            _borderImage.enabled = true;
        }

        public void OnPointerClick(BaseEventData pData)
        {
            if (_empty)
                return;
            PointerEventData pointerData = (PointerEventData)pData;
            if (pointerData.button == PointerEventData.InputButton.Right)
            {
                OnItemRightClicked?.Invoke(this);
            }
            else
            {
                OnItemClicked?.Invoke(this);
            }
        }


        #endregion
    }
}
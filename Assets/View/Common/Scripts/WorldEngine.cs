﻿using System;
using System.Collections.Generic;
using System.IO;
using Assets.View.Common.Scripts.Model.Player;
using Assets.View.Common.Scripts.Model.Util;
using Assets.View.Items.Scripts.Controllers;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace Assets.View.Common.Scripts
{
    public class WorldEngine: MonoBehaviour
    {
        #region Serialized fields
        
        [SerializeField] private GameObject _playerPrefab;

        [SerializeField] private GameObject _playerGo;
        [SerializeField] private List<GameObject> _items;
        private List<GameObject> _itemsClone;
        private List<GameObject> _enemiesPrefab;
        private List<GameObject> _enemiesPrefabClone;
        private GameObject _currentEnnemy;
        private int playerKeys;
        #endregion

        #region Singleton
        public static WorldEngine Instance;
        #endregion

        #region Unity Methods

        public void Start()
        {
            _itemsClone = new List<GameObject>();
            GameEngine.Instance.InventoryInit();
            _playerGo = Instantiate(_playerPrefab, transform);
            _playerGo.GetComponent<PickUpSystem>().SetInventory(GameEngine.Instance.GetInventoryController().GetInventoryModel());
            SetPlayerSprite();
            _playerGo.transform.position = new Vector3(-15.63f, 8.97f, 10f);

            List<GameObject> _itemPrefabs = GameEngine.Instance.GetItemPrefabs();
            _itemsClone.Add(Instantiate(_itemPrefabs[0], new Vector3(-6.93f, 8f, 0f), Quaternion.identity, transform));
            string path = Application.persistentDataPath + "/data/player" + ".json";
            if (File.Exists(path))
            {
                PlayerWorldData data = SaveWorldPlayer.Load();
                _playerGo.transform.position = new Vector3(data._x, data._y, data._z);
            }

            GameEngine.Instance.GetInventoryController().ResetPlayerFighters();
            foreach (GameObject playerGo in GameEngine.Instance.GetPlayerBattlePrefabs())
            {
                GameEngine.Instance.AddPlayerController(playerGo);
            }
            GameEngine.Instance.GetInventoryController().GetPlayerMenu().GetComponentInChildren<UI_EquipmentsButtonsPanel>().InitializeUI();

            _enemiesPrefab = GameEngine.Instance.GetEnnemiesPrefabs();
            _enemiesPrefabClone = new List<GameObject>();
            GameObject keys = GameObject.Find("Keys");
            GameObject key1 = Instantiate(_enemiesPrefab[0], new Vector3(-8.5f, 8.5f, 4.957522f), Quaternion.identity, keys.transform);
            _enemiesPrefabClone.Add(key1);
            GameObject key2 = Instantiate(_enemiesPrefab[1], new Vector3(-4.85f, 10f, 4.957522f), Quaternion.identity, keys.transform);
            _enemiesPrefabClone.Add(key2);
            GameObject key3 = Instantiate(_enemiesPrefab[2], new Vector3(2f, 5.63f, 4.957522f), Quaternion.identity, keys.transform);
            _enemiesPrefabClone.Add(key3);
            GameObject key4 =  Instantiate(_enemiesPrefab[3], new Vector3(4f, 6f, 4.957522f), Quaternion.identity, keys.transform);
            _enemiesPrefabClone.Add(key4);
            
            playerKeys = GameEngine.Instance.GetKeys();
            if (playerKeys == 3)
            {
                key4.GetComponent<BoxCollider2D>().enabled = true;
            }
            else if (playerKeys == 4)
            {
                SceneManager.LoadScene("VictoryScene");
            }
           

        }
        private void Awake()
        {
            if (Instance == null)
            {
                Instance = this;
                //DontDestroyOnLoad(this);
            }
            else
            {
                Destroy(this);
            }
        }

        #endregion

        #region Getters Setters
        public GameObject GetPlayerGo()
        {
            return _playerGo;
        }

        public List<GameObject> GetEnnemiesPrefab()
        {
            return _enemiesPrefab;
        }

        public GameObject GetCurrentEnnemy()
        {
            return _currentEnnemy;
        }

        public void SetCurrentEnnemy(GameObject pEnnemy)
        {
            _currentEnnemy = pEnnemy;
        }
        public List<GameObject> GetEnnemiesPrefabsClone()
        {
            return _enemiesPrefabClone;
        }

        public List<GameObject> GetItemsClone()
        {
            return _itemsClone;
        }
        public void SetPlayerSprite()
        {
            Sprite sprite = GameEngine.Instance.GetPlayerBattlePrefabs()[0].GetComponent<SpriteRenderer>().sprite;
            _playerGo.GetComponent<SpriteRenderer>().sprite = sprite;
            _playerGo.GetComponent<Animator>().runtimeAnimatorController =
                GameEngine.Instance.GetPlayerBattlePrefabs()[0].GetComponent<Animator>().runtimeAnimatorController;
            _playerGo.GetComponent<SpriteRenderer>().sortingOrder =
                _playerPrefab.GetComponent<SpriteRenderer>().sortingOrder;
        }

        #endregion

        [Serializable]
        public class PlayerWorldData
        {
            public float _x;
            public float _y;
            public float _z;
            public PlayerWorldData(Player player)
            {
                _x = player.transform.position.x;
                _y = player.transform.position.y;
                _z = player.transform.position.z;
            }
        }
    }
}

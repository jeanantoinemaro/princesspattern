﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Assets.View.Common.Scripts.Model.Map;
using Assets.View.Common.Scripts.ScriptableObjects;
using Assets.View.Common.Scripts.ScriptableObjects.AdvancedCharacter;
using Assets.View.Items.Scripts.Models;
using UnityEngine;

namespace Assets.View.Common.Scripts.Model.Fighter.Player
{
    [Serializable]
    public class PlayerFighter : Fighter
    {
        #region Private fields
        private int _experiences;
        private int _maxExperiences;
        private bool _isReady;

        // tests
        private EquipmentsModel _equipments;
        // tests

        //private bool _selectedForUI;
        [SerializeField] private PlayerFighter _player;
        #endregion

        public PlayerFighter(string name, AdvancedCharacter character) : base(name, character)
        {
            _maxExperiences = 10;
        }
        #region Public methods
        public string AddExperiences(int pExperience)
        {
            string levelUpMessage = null;
            _experiences += pExperience;
            if (IsReadyToLevelUp())
            {
               levelUpMessage = "You gain a level ! You are now level ";
               levelUpMessage +=  LevelUp();
            }
            return levelUpMessage;
        }
        #endregion

        #region Private methods
        private bool IsReadyToLevelUp() => _experiences >= _maxExperiences;
        private string LevelUp()
        {
            _level++;
            _experiences -= _maxExperiences;
            _maxExperiences *= 2;

            switch (_level % 3)
            {
                case 0:
                    GetCharacter().UpgradeDefense();
                    return _level + ", your defense increases by 1";
                case 1:
                    GetCharacter().UpgradeLife();
                    return _level + ", your life increases by 10";
                case 2:
                    GetCharacter().UpgradeAttack();
                    return _level + ", your attack increases by 1";
            }
            return "";
        }
        #endregion

        #region Getters Setters
        public bool IsReady()
        {
            return _isReady;
        }

        public int GetExp()
        {
            return _experiences;
        }
        public void SetIsReady(bool pReady)
        {
            _isReady = pReady;
        }
        public void SetExp(int pExp)
        {
            _experiences = pExp;
        }

        public int GetLevel()
        {
            return _level;
        }

        public void SetLevel(int pLevel)
        {
            _level = pLevel;
        }

        public void SetMaxExperience(int pMax)
        {
            _maxExperiences = pMax;
        }

        public int GetMaxExperience()
        {
            return _maxExperiences;
        }
        //public bool GetSelectedForUI()
        //{
        //    return _selectedForUI;
        //}

        //public void SetSelectedForUI(bool pSelected)
        //{
        //    _selectedForUI = pSelected;
        //}
        #endregion
    }
}
﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

namespace Assets.View.Common.Scripts.Model.State
{
    public class CameraMove : State
    {
        public override IEnumerator Start()
        {
            yield break;
        }

        public override IEnumerator MoveCamera()
        {
            var v = Vector2.zero;
            while ((Camera.main.transform.position.x != BattleEngine.Instance.GetCurrentPlayerController().GetFighterTemplate().transform.position.x) || (Camera.main.transform.position.y != BattleEngine.Instance.GetCurrentPlayerController().GetFighterTemplate().transform.position.y))
            {
                Camera.main.transform.position = Vector2.SmoothDamp(Camera.main.transform.position,
                    BattleEngine.Instance.GetCurrentPlayerController().GetFighterTemplate().transform.position, ref v, 0f, 100f);

               yield break;

            }
            if (BattleEngine.Instance.GetCurrentPlayerController() is Controller.Characters.Players.PlayerController)
            {
                BattleEngine.Instance.SetState(new ChooseAction());
            } else
            {
                BattleEngine.Instance.SetState(new EnemyTurn());
            }

        }
    }
}
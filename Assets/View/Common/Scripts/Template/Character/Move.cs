﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

namespace Assets.View.Common.Scripts.Template.Character
{
    public class Move:MonoBehaviour
    {
        #region Unity Methods

        public void Update()
        {
            StartCoroutine(BattleEngine.Instance.GetState().MovePlayer());
        }

        public void LateUpdate()
        {
            StartCoroutine(BattleEngine.Instance.GetState().MoveCamera());
        }
        #endregion
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace Assets.View.Common.Scripts.Controller.UI
{
    public class SelectionUnit:MonoBehaviour
    {
        #region Serialize fields

        [SerializeField] private Image _sprite;
        [SerializeField] private Text _name;

        #endregion

        #region Getters Setters

        public void SetSprite(Sprite sprite)
        {
            _sprite.sprite = sprite;
        }

        public void SetName(string pName)
        {
            _name.text =  pName;
        }

        #endregion
    }
}

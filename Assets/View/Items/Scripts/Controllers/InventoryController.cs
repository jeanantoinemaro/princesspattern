using Assets.View.Items.Scripts.Models;
using Assets.View.Items.Scripts.UI;
using Assets.View.Items.Scripts.Interfaces;
using System.Collections.Generic;
using UnityEngine;
using Assets.View.Common.Scripts.Model.Fighter.Player;

namespace Assets.View.Items.Scripts.Controllers
{
    public class InventoryController
    {
        #region Private Fields

        private UI_InventoryMenu _inventoryMenu; // VIEW
        private InventoryModel _inventoryData; // MODEL
        private List<EquipmentsController> _equipmentsControllers;
        private List<PlayerFighter> _playerFighters;
        private UI_EquipmentsButtonsPanel _equipmentsButtonsPanel;
        private GameObject _playerMenu;

        #endregion

        #region Getters

        public InventoryModel GetInventoryModel()
            => _inventoryData;
        public GameObject GetPlayerMenu()
            => _playerMenu;
        public UI_InventoryMenu GetInventoryMenu()
            => _inventoryMenu;
        #endregion

        #region Setters

        public void SetEquipmentsControllers(List<EquipmentsController> pEquipmentsControllers)
            => _equipmentsControllers = pEquipmentsControllers;
        public void SetPlayerFighters(List<PlayerFighter> pPlayerFighters)
            => _playerFighters = pPlayerFighters;

        #endregion

        #region Public Methods
        public void AddEquipmentController(EquipmentsController pEquipController) => _equipmentsControllers.Add(pEquipController);
        
        public void AddPlayerFighter(PlayerFighter pPFighter) => _playerFighters.Add(pPFighter);

        public void ResetPlayerFighters() => _playerFighters = new List<PlayerFighter>();
        #endregion

        #region Constructors

        public InventoryController(Object pMainPlayerObject, Object pPlayerMenu, InventoryModel pInventoryModel, List<PlayerFighter> pPlayerFighters, UI_EquipmentsButtonsPanel pEquipmentsButtonsPanel)
        {
            _inventoryMenu = (pPlayerMenu as GameObject).GetComponent<UI_InventoryMenu>(); // link the instanciated inventory menu gameobject to this attribute
            _playerMenu = (GameObject) pPlayerMenu;
            
            _inventoryData = pInventoryModel; // set inventory model

            _playerFighters = pPlayerFighters;

            _equipmentsButtonsPanel = pEquipmentsButtonsPanel;
            _equipmentsControllers = new List<EquipmentsController>();

            PrepareUI();
            PrepareInventoryData();
        }

        #endregion

        #region Private Methods

        private void PrepareUI()
        {
            _inventoryMenu.InitializeInventoryUI(_inventoryData.GetSize());
            _inventoryMenu.OnDetailsRequested += HandleDetailsRequested;
            _inventoryMenu.OnItemActionRequested += HandleItemActionRequest;
        }

        private void PrepareInventoryData()
        {
            _inventoryData.OnInventoryUpdated += UpdateInventoryUI;
        }

        private void UpdateInventoryUI(Dictionary<int, InventoryItem> pInventoryState)
        {
            _inventoryMenu.ResetAllItems();
            foreach (var item in pInventoryState)
            {
                _inventoryMenu.UpdateData(item.Key, item.Value.item.GetIcon(), item.Value.quantity);
            }
        }

        private void HandleDetailsRequested(int pItemIndex)
        {
            InventoryItem inventoryItem = _inventoryData.GetItemAt(pItemIndex);
            if (inventoryItem.IsEmpty)
            {
                _inventoryMenu.ResetSelection();
                return;
            }
            Item item = inventoryItem.item;
            _inventoryMenu.UpdateDetails(pItemIndex, item.GetIcon(), item.name, "");
        }

        private void HandleItemActionRequest(int pItemIndex)
        {
            InventoryItem inventoryItem = _inventoryData.GetItemAt(pItemIndex);
            if (inventoryItem.IsEmpty)
                return;

            int index = _equipmentsButtonsPanel.GetActiveEquipmentsContentPanel(); // get the index of the current active equipments content panel

            if (inventoryItem.item is IConsumable)
            {
                _inventoryData.RemoveItem(pItemIndex, 1);
                (inventoryItem.item as ConsumableItem).ConsumeAffectCharacter(_playerFighters[index]);
                _equipmentsControllers[index].UpdateStatsUI(); // update stats visually
            }
            if (inventoryItem.item is IEquipable)
            {
                _inventoryData.RemoveItem(pItemIndex, 1);
                (inventoryItem.item as EquipmentItem).EquipAffectCharacter(_playerFighters[index]);

                EquipmentItem oldEquipment = _equipmentsControllers[index].GetEquipmentsModel().AddEquipment(inventoryItem.item);
                if (oldEquipment != null) // if an equipment was already on the same slot we add it to the inventory
                {
                    _inventoryData.AddItem(oldEquipment, 1);
                }
            }
        }

        #endregion
    }
}
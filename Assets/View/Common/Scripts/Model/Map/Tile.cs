﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

namespace Assets.View.Common.Scripts.Model.Map
{
    public class Tile: MonoBehaviour
    {
        [SerializeField] private SpriteRenderer _renderer;
        [SerializeField] private GameObject _highlight;

        public void OnMouseEnter()
        {
            _highlight.SetActive(true);
        }

        public void OnMouseExit()
        {
            _highlight.SetActive(false);
        }

        public SpriteRenderer GetRenderer()
        {
            return _renderer;
        }
    }
}

using System.Collections.Generic;
using Assets.View.Common.Scripts.Controller.Characters.Players;
using Assets.View.Common.Scripts.Enum;
using Assets.View.Common.Scripts.Model.Fighter.Player;
using Assets.View.Common.Scripts.ScriptableObjects.AdvancedCharacter;
using Assets.View.Common.Scripts.Template.Character;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

namespace Assets.View.Common.Scripts.Controller.UI
{
    public class CharacterSelectionController : MonoBehaviour
    {
        #region Serialize fields

        [SerializeField] private List<AdvancedCharacter> _players;
        [SerializeField] private Image _attackBar;
        [SerializeField] private Image _defenseBar;
        [SerializeField] private Image _lifeBar;
        [SerializeField] private Image _apBar;
        [SerializeField] private SelectionUnit _selectionUI;
        private AdvancedCharacter _currentSelection;
        private int _currentIndex = 0;

        #endregion

        #region Public methods

        public void Next()
        {
            if (_currentIndex == _players.Count - 1)
            {
                return;
            }
            _currentIndex++;
            UpdateCurrentSelection(_currentIndex);
        }

        public void Prev()
        {
            if (_currentIndex == 0)
            {
                return;
            }
            _currentIndex--;
            UpdateCurrentSelection(_currentIndex);
        }
        public void Select()
        {
            //GameEngine.Instance.GetPlayers().Insert(0,_currentSelection);
            GameEngine.Instance.GetPlayerBattlePrefabs().Insert(0, _currentSelection.GetAssociatedPrefab());
            SceneManager.LoadScene("World");
        }

        public void UpdateCurrentSelection(int currentIndex)
        {
            _currentSelection = _players[_currentIndex];
            _selectionUI.SetName(_currentSelection.name);
            _selectionUI.SetSprite(_currentSelection.GetSprite());
            _attackBar.fillAmount = (float) _currentSelection.GetBaseAttack()/30;
            _defenseBar.fillAmount = (float)_currentSelection.GetBaseDefense() / 20;
            _lifeBar.fillAmount = (float) (_currentSelection.GetBaseLife() - 50) / 100;
            _apBar.fillAmount = (float)_currentSelection.GetActionPoint() / 5;
        }
        #endregion

        #region Unity Methods

        public void Start()
        {
            UpdateCurrentSelection(_currentIndex);
        }
        #endregion


    }
}

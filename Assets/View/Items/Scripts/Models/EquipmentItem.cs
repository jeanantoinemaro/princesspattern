using UnityEngine;
using Assets.View.Items.Scripts.Enums;
using Assets.View.Items.Scripts.Interfaces;
using Assets.View.Common.Scripts.Model.Fighter.Player;

namespace Assets.View.Items.Scripts.Models
{
    [CreateAssetMenu(menuName = "Equipment", order = 2)]
    public class EquipmentItem : Item, IEquipable
    {
        #region Serialized Fields

        [SerializeField]
        private EEquipType _equipType;

        #endregion

        #region Getters

        public EEquipType GetEquipType()
        {
            return _equipType;
        }

        #endregion

        #region Setters

        public void SetEquipType(EEquipType pEquipType)
        {
            _equipType = pEquipType;
        }

        #endregion

        #region Public Methods

        public EquipmentItem()
        {
            SetType(EItemType.Equipment);
            SetMaxStackSize(1);
        }

        public bool EquipAffectCharacter(PlayerFighter pPlayer)
        {
            foreach (ModifierData data in GetModifiers())
            {
                data.StatModifier.EquipableAffectCharacter(pPlayer, data.value);
            }
            return true;
        }

        public bool UnequipAffectCharacter(PlayerFighter pPlayer)
        {
            foreach (ModifierData data in GetModifiers())
            {
                data.StatModifier.EquipableAffectCharacter(pPlayer, - data.value);
            }
            return true;
        }

        #endregion
    }
}

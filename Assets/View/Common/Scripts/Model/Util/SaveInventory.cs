﻿using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Assets.View.Items.Scripts.Models;
using UnityEngine;

namespace Assets.View.Common.Scripts.Model.Util
{
    public static class SaveInventory
    {
        private static string _basePath = Application.persistentDataPath + "/data/inventory.json";
        public static void Save(Dictionary<int, InventoryItem> items)
        {
            List<ItemData> itemDatas = new List<ItemData>();
            foreach (var item in items)
            {
                if (item.Value.IsEmpty)
                {
                    continue;

                }
                itemDatas.Add(new ItemData(item.Value.item.name, item.Value.quantity, item.Key));
            }
            InventoryData data = new InventoryData(itemDatas);
            string jsonData = JsonUtility.ToJson(data);
            File.WriteAllText(_basePath, jsonData);

        }

        public static Dictionary<int, InventoryItem> Load()
        {
            string jsonData = File.ReadAllText(_basePath);
            InventoryData data = JsonUtility.FromJson<InventoryData>(jsonData);
            Dictionary<int, InventoryItem> inventory = new Dictionary<int, InventoryItem>();
            foreach (ItemData itemData in data._inventory)
            {
                foreach (var item in GameEngine.Instance._items)
                {
                    if (item.name == itemData._name)
                    {
                        InventoryItem newItem = new InventoryItem
                        {
                            item = item,
                            quantity = itemData._quantity
                        };
                        inventory.Add(itemData._position, newItem);
                    }
                }

            }
            return inventory;
        }
    }
}

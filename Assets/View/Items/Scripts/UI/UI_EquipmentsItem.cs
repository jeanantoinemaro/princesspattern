using System;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using Assets.View.Items.Scripts.Enums;

namespace Assets.View.Items.Scripts.UI
{
    public class UI_EquipmentsItem : MonoBehaviour
    {
        #region Serialized Fields

        [SerializeField] private Image _itemImage;

        #endregion

        #region Fields

        private EEquipType _equipmentType;
        public event Action<UI_EquipmentsItem> OnItemClicked, OnItemRightClicked;
        private bool _empty = true; // TODO

        #endregion

        #region Public Methods

        public void ResetData()
        {
            _itemImage.gameObject.SetActive(false);
            _empty = true;
        }

        public void SetData(Sprite pSprite)
        {
            _itemImage.gameObject.SetActive(true);
            _itemImage.sprite = pSprite;
            _empty = false;
        }

        public void OnPointerClick(BaseEventData pData)
        {
            if (_empty)
                return;
            PointerEventData pointerData = (PointerEventData)pData;
            if (pointerData.button == PointerEventData.InputButton.Right)
            {
                OnItemRightClicked?.Invoke(this);
            }
            else
            {
                OnItemClicked?.Invoke(this);
            }
        }

        #endregion

        #region Unity Methods

        public void Awake()
        {
            ResetData();
        }

        #endregion
    }
}
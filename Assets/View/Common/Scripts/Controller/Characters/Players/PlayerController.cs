﻿using System;
using Assets.View.Common.Scripts.Model.Map;
using Assets.View.Common.Scripts.Template.Character;
using UnityEngine;
using Assets.View.Items.Scripts.Controllers;
using PlayerFighter = Assets.View.Common.Scripts.Model.Fighter.Player.PlayerFighter;

namespace Assets.View.Common.Scripts.Controller.Characters.Players
{
    public class PlayerController : FighterController
    {

        #region Serialize fields
        private EquipmentsController _equipmentController;

        #endregion

        public PlayerController(PlayerFighter player, FighterTemplate template) : base(player, template)
        {

        }


        #region Getters Setters

        public PlayerFighter GetPlayer()
        {
            return (PlayerFighter) _fighter;
        }

        public EquipmentsController GetEquipmentsController()
        {
            return _equipmentController;
        }
        public void SetEquipmentsController(EquipmentsController ec)
        {
            _equipmentController = ec;
        }

        #endregion


        #region Public methods

        public void OnNameChanged(string pName)
        {
            string name = _fighter.GetName();
            _fighterTemplate.SetName(name);

        }

        public void SetName(string pName)
        {
            _fighter.SetName(pName);
        }

        #endregion
    }
}

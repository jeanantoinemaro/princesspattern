using TMPro;
using UnityEngine;

namespace Assets.View.Items.Scripts.UI
{
    public class UI_StatsItem : MonoBehaviour
    {
        #region Serialized Fields

        [SerializeField] private TMP_Text _statName;
        [SerializeField] private TMP_Text _statValue;

        #endregion

        #region Public Methods

        public void ResetData()
        {
            _statName.text = "";
            _statValue.text = "";
        }

        public void SetData(string pStatName, string pStatValue)
        {
            _statName.text = pStatName + ":";
            _statValue.text = pStatValue;
        }

        public void SetValue(string pStatValue)
        {
            _statValue.text = pStatValue;
        }

        #endregion

        #region Unity Methods

        public void Awake()
        {
            ResetData();
        }

        #endregion
    }
}
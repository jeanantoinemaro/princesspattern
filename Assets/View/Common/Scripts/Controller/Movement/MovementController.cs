﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;
using UnityEngine.Tilemaps;

namespace Assets.View.Common.Scripts.Controller.Movement
{
    public class MovementController:MonoBehaviour
    {
        #region Private fields

        private Vector2 _target;
        private Tilemap _tileMap;
        private Vector2 _v;
        [SerializeField] private float speed = 5f;
            #endregion
        #region Unity Methods

        public void Awake()
        {
            _tileMap = FindObjectOfType<Tilemap>();
        }
        public void Start()
        {
            _v = Vector2.zero;
            _target = transform.position;
        }
        public void Update()
        {
            if (Input.GetMouseButtonDown(0))
            {
                _target = UnityEngine.Camera.main.ScreenToWorldPoint(Input.mousePosition);

            }
            
            transform.position = Vector2.MoveTowards(transform.position, _target , speed*Time.deltaTime);
                    
        }
        #endregion
    }
}

using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Assets.View.Common.Scripts.Controller.Characters;
using Assets.View.Common.Scripts.Controller.Characters.Players;
using Assets.View.Common.Scripts.Controller.UI;
using Assets.View.Common.Scripts.Model.Map;
using Assets.View.Common.Scripts.Template.Character;
using UnityEngine;
using UnityEngine.SocialPlatforms;

namespace Assets.View.Common.Scripts.Model.State
{
    public class DispatchPlayers : State
    {

        public override IEnumerator Start()
        {
            BattleEngine.Instance.BattleUI.SetInfoText("Dispatch your characters");
            GameEngine.Instance.GetInventoryController().ResetPlayerFighters();
            yield break;
        }

        public override IEnumerator DispatchPlayer(TileGo tile)
        {
            if (tile.gridLocation.x >= 3  || tile.isBlocked)
            {
                yield break;
            }
            List<UICharacter> uiCharacters = GameObject.Find("DispatchPanel").GetComponent<DispatchMenuController>()
                .GetUiCharacters();
            //var playerPrefabs = BattleEngine.Instance.GetPlayerPrefabs();
            if (uiCharacters.Count != 0)
            {
                foreach (var uc in uiCharacters)
                {
                    if (!uc.GetSelected())
                    {
                        continue;
                    }

                    PlayerController playerController = BattleEngine.Instance.SpawnOnTile(uc.GetPlayerPrefab(), tile);
                    playerController.SetStandingOnTile(tile);
                    playerController.GetFighterTemplate().GetSprite().sortingOrder = tile.GetComponent<SpriteRenderer>().sortingOrder;
                    Cursor.SetCursor(null, Vector2.one, CursorMode.Auto);
                    uc.GetPlayerPrefab().SetActive(true);
                    uc.SetSelected(false);
                    uc.gameObject.SetActive(false);
                }
            }

            if (BattleEngine.Instance.GetPlayersNumber() == 0)
            {
                GameEngine.Instance.GetInventoryController().GetPlayerMenu().GetComponentInChildren<UI_EquipmentsButtonsPanel>().InitializeUI();
                BattleEngine.Instance.GetCanvas().transform.GetChild(0).gameObject.SetActive(false);
                BattleEngine.Instance.BattleUI.DisableInfoText();
                BattleEngine.Instance.SetState(new Begin());
            }
            yield break;
    }

        public override IEnumerator SelectController(FighterController fController)
        {
            BattleEngine.Instance.SetCurrentPlayerController(fController as PlayerController);
            yield break;
        }
    }
}

using UnityEngine;

namespace Assets.View.Common.Scripts.ScriptableObjects.Moves
{
    [CreateAssetMenu(fileName = "New Action", menuName = "Action/Attack")]
    public class AttackAction : BattleAction
    {
        private int _damage;

        [SerializeField]
        private SpriteRenderer _sprite;

        public int GetDamage() => _damage;

        public void SetDamage(int pDamage) => _damage = pDamage;

        //public void CalculateDamage()
        //{
        //    int pAttack = GetPlayer()
        //        .GetAdvancedCharacter()
        //        .GetAttack();
        //    _damage = GetAmount() * pAttack;
        //}

        //public void DealDamage()
        //{
        //    CalculateDamage();
        //    // TODO
        //}


        //public override void Attack(AdvancedCharacter.AdvancedCharacter target)
        //{
        //    target.Damage(GetFighter().GetCharacter().GetAttack());
        //}
    }
}


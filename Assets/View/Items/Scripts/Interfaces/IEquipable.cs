﻿using Assets.View.Common.Scripts.Model.Fighter.Player;
using UnityEngine;

namespace Assets.View.Items.Scripts.Interfaces
{
    interface IEquipable
    {
        bool EquipAffectCharacter(PlayerFighter pPlayer);
        bool UnequipAffectCharacter(PlayerFighter pPlayer);
    }
}

﻿using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Assets.View.Common.Scripts.Controller.Characters;
using Assets.View.Common.Scripts.Controller.Characters.Players;
using Assets.View.Common.Scripts.Model.Fighter.Player;
using Assets.View.Common.Scripts.Model.Util;
using Assets.View.Common.Scripts.ScriptableObjects;
using Assets.View.Common.Scripts.ScriptableObjects.AdvancedCharacter;
using Assets.View.Common.Scripts.Template.Character;
using Assets.View.Items.Scripts.Controllers;
using Assets.View.Items.Scripts.Models;
using Assets.View.Items.Scripts.UI;
using Assets.View.Common.Scripts.Enum;
using System.IO;
using Assets.View.Common.Scripts.Model.Util;
using UnityEngine;
using UnityEngine.Serialization;

namespace Assets.View.Common.Scripts
{
    public class GameEngine : MonoBehaviour
    {
        #region Serialized fields
        [SerializeField] private List<GameObject> _playerBattlePrefabs;
        [SerializeField] private List<PlayerController> _playerControllers;
        [SerializeField] private List<AdvancedCharacter> _characterSos;
        [SerializeField] private List<InventoryModel> _inventories;
        [SerializeField] public List<Item> _items;
        [SerializeField] public List<GameObject> _itemPrefabs;
        [SerializeField] private List<GameObject> _enemiesPrefab;
        #endregion

        #region Private Fields

        private InventoryController _inventoryController;
        private List<EquipmentsController> _equipmentsControllers;
        private int _keys;

        #endregion

        #region Singleton
        public static GameEngine Instance;
        #endregion

        #region Unity Methods

        public void Start()
        {
            _playerControllers = new List<PlayerController>();
            _equipmentsControllers = new List<EquipmentsController>();
            _keys = 0;
        }
        private void Awake()
        {
            if (Instance == null)
            {
                Instance = this;
                foreach (GameObject ennemyGo in _enemiesPrefab)
                {
                    ennemyGo.SetActive(true);
                }

                foreach (GameObject itemGo in _itemPrefabs)
                {
                    itemGo.SetActive(true);
                }

                SaveFighters.DeleteAllFighters();
                DontDestroyOnLoad(this);
            }
            else
            {
                Destroy(this);
            }
        }
        #endregion
        #region Public Methods

        public void InventoryInit()
        {
            List<PlayerFighter> players = new List<PlayerFighter>();
            
            foreach (GameObject playerGo in _playerBattlePrefabs)
            {
                FighterTemplate template = playerGo.GetComponent<FighterTemplate>();
                string name = System.Enum.GetName(typeof(PlayerCharacterEnum), template.GetPlayerCharacterEnum());
                players.Add(new PlayerFighter(template.GetName(), GetCharacter(name)));
            }
               
            Object mainPlayerObject = Instantiate(Resources.Load("PFTestInstantiatePlayer"), // main player on world map
                new Vector3(-2, -1, 10), Quaternion.identity);
            (mainPlayerObject as GameObject).gameObject.SetActive(false);

            // one inventory UI instantiation with the whole PlayerItemsMenu
            Object playerItemsMenuObject = Instantiate(Resources.Load("PlayerItemsMenu"),
                GameObject.Find("Canvas").transform);

            // set inventory UI in input handler script (PlayerMovement)
            (mainPlayerObject as GameObject).GetComponent<PlayerMovement>()
                .SetInventoryMenu((playerItemsMenuObject as GameObject).GetComponent<UI_InventoryMenu>());

            // get Equipments Buttons Panel gameobject which handles switching between tabs to pass it to inventory controller
            UI_EquipmentsButtonsPanel equipmentsButtonsPanel = (playerItemsMenuObject as GameObject)
                .GetComponentInChildren<UI_EquipmentsButtonsPanel>();

            string path = Application.persistentDataPath + "/data/inventory.json";
            InventoryModel inventoryModel = null;
            if (File.Exists(path))
            {
                Dictionary<int, InventoryItem> data = SaveInventory.Load();
                inventoryModel = Instantiate(_inventories[1]);
                foreach (var item in data)
                {
                    inventoryModel.AddItemToSlot(item.Key, item.Value);
                }

            }
            else
            {
                inventoryModel = Instantiate(_inventories[0]);
            }
            // one inventory instantiation
            _inventoryController = new InventoryController(mainPlayerObject, playerItemsMenuObject, inventoryModel, players, equipmentsButtonsPanel);
        }

        public PlayerController AddPlayerController(GameObject playerGo)
        {
            FighterTemplate template = playerGo.GetComponent<FighterTemplate>();
            string name = System.Enum.GetName(typeof(PlayerCharacterEnum),
                template.GetPlayerCharacterEnum());
            PlayerFighter playerFighter = null;

            string path = Application.persistentDataPath + "/data/" +
                          template.GetName() + ".json";
            if (File.Exists(path))
            {
                playerFighter = SaveFighters.Load(name, template.GetName());
            }
            else
            {
                playerFighter = new PlayerFighter(template.GetName(), GetCharacter(name));
            }
            Object equipmentsPanelObject = Instantiate(Resources.Load("EquipmentsContentPanel"), 
                _inventoryController.GetPlayerMenu().GetComponentsInChildren<Transform>(true)[1].gameObject.transform);
            EquipmentsController equipController = new EquipmentsController(playerFighter, equipmentsPanelObject, _inventoryController);
            if (File.Exists(path))
            {
                List<EquipmentItem> equipments = SaveFighters.LoadEquipment(template.GetName());
                foreach (EquipmentItem item in equipments)
                {
                    equipController.GetEquipmentsModel().AddEquipment(item);
                }
            }
            _equipmentsControllers.Add(equipController);
            _inventoryController.AddEquipmentController(equipController);
            _inventoryController.AddPlayerFighter(playerFighter);

            UI_EquipmentsButtonsPanel equipmentsButtonsPanel = _inventoryController.GetPlayerMenu().GetComponentInChildren<UI_EquipmentsButtonsPanel>();
            equipmentsButtonsPanel.AddEquipmentsContent(equipmentsPanelObject as GameObject, playerFighter);


            PlayerController playerController = new PlayerController(playerFighter, template);
            playerController.SetEquipmentsController(equipController);

            if (null != BattleEngine.Instance)
            {
                playerController.GetFighterTemplate().Selected += playerController.OnSelected;
                playerController.Selected += BattleEngine.Instance.SetSelectedController;
                BattleEngine.Instance.GetPlayerControllers().Add(playerController);
            }
            _playerControllers.Add(playerController);
            return playerController;

        }

        public void AddKey()
        {
            _keys += 1;
        }

        #endregion
        #region Getters

        public List<GameObject> GetPlayerBattlePrefabs() => _playerBattlePrefabs;

        public List<GameObject> GetItemPrefabs() => _itemPrefabs;
        public List<PlayerController> GetPlayerControllers() => _playerControllers;

        public AdvancedCharacter GetCharacter(string character)
        {
            return Instantiate( _characterSos.Where(_character => _character.name == character).FirstOrDefault());
        }
        public List<GameObject> GetEnnemiesPrefabs()
        {
            return _enemiesPrefab;
        }

        public InventoryController GetInventoryController() => _inventoryController;
        public List<EquipmentsController> GetEquipmentsController() => _equipmentsControllers;
        public List<InventoryModel> GetInventoryModels() => _inventories;
        public List<Item> GetListItems() => _items;

        public int GetKeys() => _keys;

        #endregion

        #region Setters
        public void SetPlayerControllers(List<PlayerController> pControllers) =>  _playerControllers = pControllers;
        #endregion
    }
}

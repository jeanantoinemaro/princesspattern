﻿using System.Collections;
using UnityEngine;

namespace Assets.View.Common.Scripts.Template.Ennemy
{
    public class MoveEnemy : MonoBehaviour
    {

        #region Unity Methods

        public void Update()
        {
            BattleEngine.Instance.GetState().MoveEnnemyTo();
        }

        public void LateUpdate()
        {
            StartCoroutine(BattleEngine.Instance.GetState().MoveCamera());
        }
        #endregion
    }
}
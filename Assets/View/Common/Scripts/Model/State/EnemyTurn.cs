﻿using Assets.View.Common.Scripts.Model.Map;
using System.Collections;
using System.Collections.Generic;
using Assets.View.Common.Scripts.Controller.Characters.Ennemy;
using Assets.View.Common.Scripts.Controller.Characters.Players;
using Assets.View.Common.Scripts.Controller.Characters;
using BattleAction = Assets.View.Common.Scripts.ScriptableObjects.BattleAction;
using UnityEngine;

namespace Assets.View.Common.Scripts.Model.State
{
    public class EnemyTurn : State
    {
        private List<EnnemyController> _ennemyControllers;
        private EnnemyController _currentEnemyController;
        private List<TileGo> path;
        private TileGo _destinationTile;
        private FighterController _target;
        public override IEnumerator Start()
        {

            if (BattleEngine.Instance.AreAllPlayersDead())
            {
                BattleEngine.Instance.SetState(new LostState());
                yield break;
            }
            if (BattleEngine.Instance.AreAllEnemiesDead())
            {
                BattleEngine.Instance.SetState(new WonState());
                yield break;
            }

            if (BattleEngine.Instance.HaveAllEnemiesActed())
            {
                BattleEngine.Instance.BattleUI.SetDialogText("Your turn");
                yield return new WaitForSeconds(1f);
                BattleEngine.Instance.BattleUI.DisableDialogText();
                BattleEngine.Instance.NewPlayerTurn();
                BattleEngine.Instance.SetCurrentPlayerController(null);
                BattleEngine.Instance.SetState(new SelectNextPlayer());
                yield break;
            }

            BattleEngine.Instance.BattleUI.SetDialogText("Enemy turn");
            yield return new WaitForSeconds(1f);
            BattleEngine.Instance.BattleUI.DisableDialogText();
            _ennemyControllers = BattleEngine.Instance.GetEnnemyControllers();

            foreach (EnnemyController ec in _ennemyControllers)
            {
                if (ec.IsReady())
                {
                    _destinationTile = GetMinDistance(ec.GetStandingOnTile());
                    _currentEnemyController = ec;
                    BattleEngine.Instance.SetCurrentPlayerController(ec);

                    break;
                }
            }

            yield break;
        }



        #region Action Methods
        public void SelectAction()
        {
            List<BattleAction>  actions = _currentEnemyController.GetFighter().GetCharacter().GetActions();
            List<BattleAction> legalActions = new List<BattleAction>();
            BattleAction currentAction;
            int distance = FindDistanceToTarget();

            foreach (BattleAction action in actions)
            {
                if (action.GetRange() >= distance)
                {
                    legalActions.Add(action);
                }
            }

            if (legalActions.Count > 0)
            {
                int random = Random.Range(0, legalActions.Count);
                currentAction = legalActions[random];
                _currentEnemyController.GetFighter().GetCharacter().SetCurrentMove(currentAction);
            } else
            {
                _currentEnemyController.Defend();
            }

        }

        public void AttackPlayer()
        {
            _currentEnemyController.GetFighter().GetCharacter().Attack(_target.GetFighter());
            _currentEnemyController.GetFighterTemplate().gameObject.GetComponent<Animator>().Play("SHOOT");
        }

        private int FindDistanceToTarget()
        {
            return new PathFinder().FindStraightDistance(_currentEnemyController.GetStandingOnTile(), _target.GetStandingOnTile());
        }

        #endregion

        #region Movement Methods
        public override void MoveEnnemyTo()
        {
            if (_currentEnemyController == null)
            {
                return;
            }
            var pathFinder = new PathFinder();
            var enemy = _currentEnemyController.GetFighter();
            _destinationTile.isBlocked = false;
            path = pathFinder.FindPath(enemy.GetStandingOnTile(), _destinationTile);
            _destinationTile.isBlocked = true;
            if (path.Count > 1 && _currentEnemyController.CanMove())
            {
                MoveAlongPath();
            } else
            {
                _currentEnemyController.GetFighterTemplate().gameObject.GetComponent<Animator>().Play("IDLE");
                SelectAction();
                if (!_currentEnemyController.GetFighter().IsDefending())
                {
                    AttackPlayer();
                }
                _currentEnemyController.SetIsReady(false);
                BattleEngine.Instance.SetState(new CameraMove());
            }
            return;

        }

        private void MoveAlongPath()
        {
            var enemy = _currentEnemyController.GetFighterTemplate();
            enemy.gameObject.GetComponent<Animator>().Play("WALK");
            var speed = 1f;
            var step = speed * Time.deltaTime;

            float zIndex = path[0].transform.position.z;

            enemy.transform.position = Vector2.MoveTowards(enemy.transform.position, path[0].transform.position, step);
            enemy.transform.position = new Vector3(enemy.transform.position.x, enemy.transform.position.y, zIndex);

            if (Vector2.Distance(enemy.transform.position, path[0].transform.position) < 0.00001f)
            {
                PositionCharacterOnLine(path[0]);
                path.RemoveAt(0);
                _currentEnemyController.DecrementMovementPoints();
            }

        }

        private void PositionCharacterOnLine(TileGo tile)
        {
            var enemyController = _currentEnemyController;
            enemyController.GetFighterTemplate().transform.position = new Vector3(tile.transform.position.x, tile.transform.position.y + 0.0001f, tile.transform.position.z);
            enemyController.GetFighterTemplate().GetSprite().sortingOrder = tile.GetComponent<SpriteRenderer>().sortingOrder;
            enemyController.SetStandingOnTile(tile);
        }

        private TileGo GetMinDistance(TileGo start)
        {
            int min = 100;
            TileGo destinationTile = null;
            var pathFinder = new PathFinder();

            foreach (PlayerController playerController in BattleEngine.Instance.GetPlayerControllers())
            {
                if (!playerController.IsDead())
                {
                    TileGo playerTile = playerController.GetStandingOnTile();
                    if (pathFinder.GetManhattenDistance(start, playerTile) < min)
                    {
                        min = pathFinder.GetManhattenDistance(start, playerTile);
                        destinationTile = playerTile;
                        _target = playerController;
                    }
                }
            }
            return destinationTile;
        }
        #endregion
    }
}

using System;
using System.Collections.Generic;
using UnityEngine;
using Assets.View.Items.Scripts.Enums;

namespace Assets.View.Items.Scripts.Models
{
    [CreateAssetMenu]
    public class EquipmentsModel : ScriptableObject // scriptable car on a besoin d'avoir plusieurs �quipments
    {
        #region Serialized Fields

        private List<EquipmentItem> _currentEquipments;
        private int _size = 4;

        #endregion

        #region Events

        public event Action<Dictionary<EEquipType, EquipmentItem>> OnEquipmentsUpdated;

        #endregion

        #region Getters

        public int GetSize() => _size;
        public List<EquipmentItem> GetCurrentEquipments() => _currentEquipments;

        #endregion

        #region Private Methods

        private void InformAboutChange()
        {
            OnEquipmentsUpdated?.Invoke(GetCurrentEquipmentsState());
        }

        #endregion

        #region Public Methods

        public void Initialize()
        {
            _currentEquipments = new List<EquipmentItem>();
            for (int i = 0; i < _size; i++)
            {
                _currentEquipments.Add(CreateInstance<EquipmentItem>());
            }
        }

        public EquipmentItem AddEquipment(Item pItem)
        {
            EquipmentItem oldItem = null;
            int index = (int)(pItem as EquipmentItem).GetEquipType();

            if (_currentEquipments[index].GetIcon() != null)
            {
                oldItem = _currentEquipments[index];
            }
            _currentEquipments[index] = (EquipmentItem)pItem;
            InformAboutChange();

            return oldItem;
        }

        public EquipmentItem GetEquipmentAt(int pItemIndex)
        {
            return _currentEquipments[pItemIndex];
        }

        public void RemoveEquipment(int pItemIndex)
        {
            if (_currentEquipments.Count > pItemIndex)
            {
                if (_currentEquipments[pItemIndex].GetIcon() == null)
                    return;
                _currentEquipments[pItemIndex] = CreateInstance<EquipmentItem>();
                InformAboutChange();
            }
        }

        public Dictionary<EEquipType, EquipmentItem> GetCurrentEquipmentsState()
        {
            Dictionary<EEquipType, EquipmentItem> returnValue = new Dictionary<EEquipType, EquipmentItem>();
            for (int i = 0; i < _currentEquipments.Count; i++)
            {
                if (_currentEquipments[i].GetIcon() == null)
                    continue;
                returnValue[_currentEquipments[i].GetEquipType()] = _currentEquipments[i];
            }
            return returnValue;
        }

        #endregion
    }
}
﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine.Tilemaps;
using Assets.View.Common.Scripts.Model.Map;
using UnityEngine;

namespace Assets.View.Common.Scripts.Model.State
{
    public class Begin:State
    {
        public override IEnumerator Start()
        {
            var tileMap = MapEngine.Instance.GetComponentInChildren<Tilemap>();
            int height = MapEngine.Instance.GetHeight();
            int depth = MapEngine.Instance.GetDepth();
            for (int x = 0; x < 3; x++)
            {
                for (int y = 0; y < height; y++)
                {
                    for (int z = 0; z < depth; z++)
                    {
                        var tileLocation = new Vector3Int(x, y, z);
                        MapEngine.Instance.GetTileAtPosition(tileLocation).GetComponent<TileGo>().DeactivateHighlight2();

                    }
                }
            }
            BattleEngine.Instance.InitBattleHP();
            BattleEngine.Instance.BattleUI.SetDialogText("Battle start !");
            yield return new WaitForSeconds(2f);
            BattleEngine.Instance.BattleUI.DisableDialogText();

            BattleEngine.Instance.NewPlayerTurn();
            BattleEngine.Instance.SetState(new SelectNextPlayer());
            yield break;
        }
    }
}

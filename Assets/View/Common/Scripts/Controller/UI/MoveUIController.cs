﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;
using UnityEngine.EventSystems;
using BattleAction = Assets.View.Common.Scripts.ScriptableObjects.BattleAction;

namespace Assets.View.Common.Scripts.Controller.UI
{
    public class MoveUIController:MonoBehaviour, IPointerDownHandler
    {
        #region Serialized Fields

        [SerializeField] private BattleAction _move;
        #endregion
        #region Unity Method

        public void OnPointerDown(PointerEventData eventData)
        {
            if (null == _move)
            {
                return;
            }
            BattleEngine.Instance.GetCurrentPlayerController().GetFighter().GetCharacter().SetCurrentMove(_move);
            MapEngine.Instance.SetTilesTo(true, _move.GetRange());
            StartCoroutine(BattleEngine.Instance.GetState().AttackChoice());
            BattleEngine.Instance.GetMoveContainer().SetActive(false);
            BattleEngine.Instance.GetActions().SetActive(false);
        }

        #endregion
        #region Setters

        public void SetMove(BattleAction pAction)
        {
            _move = pAction;
        }
        #endregion
    }
}

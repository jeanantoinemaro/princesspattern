using System;
using System.Collections.Generic;
using Assets.View.Common.Scripts.Model.Fighter;
using UnityEngine;

namespace Assets.View.Common.Scripts.ScriptableObjects.AdvancedCharacter
{
    [Serializable]
    [CreateAssetMenu(fileName = "New Character", menuName = "Character", order = 1)]
    public class AdvancedCharacter : Character
    {
        #region Fields

        [Header("Advanced Character settings")]
        [SerializeField] private int _baseActionPoint;
        private int _maxActionPoint;
        private int _actionPoint;

        [SerializeField] private int _baseMovementPoint;
        private int _maxMovementPoint;
        private int _movementPoint;

        [SerializeField] private int _baseAttack;
        private int _attack;

        [SerializeField] private int _baseDefense;
        private int _defense;

        [SerializeField] private Sprite _sprite;
        [SerializeField] private GameObject _associatedPrefab;

        [Header("Moves settings")]
        [SerializeField] private List<BattleAction> _moves;

        [SerializeField] private BattleAction _currentMove;


        #endregion

        #region Getters
        public int GetBaseActionPoint() => _baseActionPoint;
        public int GetMaxActionPoint() => _maxActionPoint;
        public int GetActionPoint() => _actionPoint;
        public int GetBaseMovementPoint() => _baseMovementPoint;
        public int GetMaxMovementPoint() => _maxMovementPoint;
        public int GetMovementPoint() => _movementPoint;
        public int GetBaseAttack() => _baseAttack;
        public int GetAttack() => _attack;
        public int GetBaseDefense() => _baseDefense;
        public int GetDefense() => _defense;
        public Sprite GetSprite() => _sprite;
        public GameObject GetAssociatedPrefab() => _associatedPrefab;

        public List<BattleAction> GetActions() => _moves;
        public BattleAction GetCurrentMove() => _currentMove;
        #endregion

        #region Setters
        public void SetMaxActionPoint(int pMaxActionPoint) => _maxActionPoint = pMaxActionPoint;
        public void SetActionPoint(int pActionPoint) => _actionPoint = pActionPoint;
        public void SetMaxMovementPoint(int pMaxMovementPoint) => _maxMovementPoint = pMaxMovementPoint;
        public void SetMovementPoint(int pMovementPoint) => _movementPoint = pMovementPoint;
        public void SetAttack(int pAttack) => _attack = pAttack;
        public void SetDefense(int pDefense) => _defense = pDefense;
        #endregion

        public void SetCurrentMove(BattleAction pMove)
        {
            _currentMove = pMove;
        }
        public bool UseActionPoint(int pActionPoint)
        {
            if (_actionPoint >= pActionPoint)
            {
                _actionPoint -= pActionPoint;
                return true;
            }
            else
                return false;
        }

        public void ResetMovementPoints()
        {
            _maxMovementPoint = _baseMovementPoint;
            _movementPoint = _maxMovementPoint;
        }
        public void DecrementMovementPoints()
        {
            if (_movementPoint > 0)
            {
                _movementPoint--;
            }
        }

        public void ResetActionPoints()
        {
            _maxActionPoint = _baseActionPoint;
            _actionPoint = _maxActionPoint;
        }

        public void ResetAdvancedCharacter()
        {

            if (_attack == 0)  _attack = _baseAttack;
            if (_defense == 0)  _defense = _baseDefense;

            ResetMovementPoints();
            ResetActionPoints();
            ResetCharacter();
        }

        public new void OnEnable() => ResetAdvancedCharacter();


        public void Attack(Fighter target)
        {
            int defenseModifier = (target.IsDefending() ? 1 : 2);
            target.GetCharacter().Damage(Math.Max(GetAttack() + _currentMove.GetAmount() - (target.GetCharacter().GetDefense() / defenseModifier), 0));
        }

        public void Heal(Fighter target)
        {
            target.GetCharacter().Heal(_currentMove.GetAmount());

        }

        public void UpgradeAttack()
        {
            _baseAttack++;
            _attack++;
            ResetAdvancedCharacter();
        }

        public void UpgradeDefense()
        {
            _baseDefense++;
            _defense++;
            ResetAdvancedCharacter();
        }

        public override string ToString()
        {
            return "AdvancedCharacter : " +
                "_maxActionPoint : " + _maxActionPoint.ToString() + 
                "_actionPoint : " + _actionPoint.ToString() + 
                "_attack" + _attack.ToString() + 
                "; _defense: " + _defense.ToString();
        }
    }
}

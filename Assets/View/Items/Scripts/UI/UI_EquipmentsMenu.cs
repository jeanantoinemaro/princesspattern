﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace Assets.View.Items.Scripts.UI
{
    public class UI_EquipmentsMenu : MonoBehaviour
    {
        #region Serialized Fields

        [SerializeField] private UI_EquipmentsItem _equipmentPrefab;
        [SerializeField] private RectTransform _contentPanel;

        #endregion

        #region Fields

        private List<UI_EquipmentsItem> _uiEquipments = new List<UI_EquipmentsItem>();
        public event Action<int> OnDetailsRequested;
        public event Action<int> OnItemActionRequested;

        #endregion

        #region Private Methods

        private void HandleItemSelection(UI_EquipmentsItem pUiEquipmentItem) // feature not implemented
        {
            //int index = uiItems.IndexOf(pUiEquipmentItem);
            //if (index == -1)
            //    return;
            //uiItems[index].Select();
            // invoke action to display item details
        }

        private void HandleUseItem(UI_EquipmentsItem pUiEquipmentItem)
        {
            int index = _uiEquipments.IndexOf(pUiEquipmentItem);
            if (index == -1)
                return;
            OnItemActionRequested?.Invoke(index);
        }

        #endregion

        #region Public Methods

        public void InitializeEquipmentsUI(int pEquipmentSize)
        {
            for (int i = 0; i < pEquipmentSize; i++)
            {
                UI_EquipmentsItem uiItem = Instantiate(_equipmentPrefab, Vector3.zero, Quaternion.identity);
                uiItem.transform.SetParent(_contentPanel);
                _uiEquipments.Add(uiItem);
                uiItem.OnItemClicked += HandleItemSelection;
                uiItem.OnItemRightClicked += HandleUseItem;
            }
        }

        public void UpdateData(int pEquipmentIndex, Sprite pEquipmentImage)
        {
            if (_uiEquipments.Count > pEquipmentIndex)
            {
                _uiEquipments[pEquipmentIndex].SetData(pEquipmentImage);
            }
        }

        public void ResetAllItems()
        {
            foreach (var item in _uiEquipments)
            {
                item.ResetData();
            }
        }

        #endregion
    }
}

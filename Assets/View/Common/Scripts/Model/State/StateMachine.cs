﻿using UnityEngine;

namespace Assets.View.Common.Scripts.Model.State
{
    public abstract class StateMachine: MonoBehaviour
    {
        protected State _state;

        public void SetState(State state)
        {
            _state = state;
             StartCoroutine(_state.Start());
        }

        public State GetState()
        {
            return _state;
        }

    }
}

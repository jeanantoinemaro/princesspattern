# Groupe de la_t 992309

# Le jeu :
Après une introduction à l’histoire (la princesse a disparu !), le joueur est invité à choisir son personnage. Il se retrouve sur un monde de jeu sur lequel il peut se déplacer à l’aide du clavier (ZQSD). 
Il peut y ramasser des objets et y rencontrer des monstres. Chaque monstre renvoie à un combat spécifique. Pour accéder au dernier niveau, celui du boss, il faut avoir réussi les 3 premiers combats. En battant le boss au dernier niveau, le joueur accède à une scène de fin concluant l’histoire.

#### Architecture :
Trois scripts principaux permettent de gérer la logique de jeu : le GameEngine, le BattleEngine et le WorldEngine (ces deux derniers correspondant aux combats ou à l’open world).


# Système de combat :
Le jeu de combat se déroule sous la forme d’un combat tour par tour sur un quadrillage avec des limites finies. Le tour du joueur se déclenche en premier, ensuite c’est le tour des ennemis. Chaque tour du joueur se décompose en plusieurs phases:
La phase de dispatch: Le joueur décide où mettre chacun de ses personnages sur une région limitée sur la map.
La phase de déplacement: Le joueur clique sur le joueur qu’il souhaite déplacer puis cliquer sur une case correspondant au potentiel case que peut atteindre le joueur. Il peut choisir de rester sur place.
La phase d'action: Le joueur choisit une action parmi Attaque et Défendre.
En cliquant sur Défendre, le joueur subit moins de dégâts lors d’une attaque.
La phase d’attaque: En cliquant sur Attaque, cette phase se déclenche, le joueur peut alors choisir sa technique et sa cible. Il peut annuler son attaque pour changer d’action en appuyant sur “Echap”.
Le combat se termine une fois que tous les ennemis sont éliminés et les personnages partagent l’expérience qui leur permet de monter de niveau et d’améliorer leurs statistiques.

#### Architecture :
Pour le système de combat, nous avons opté pour le design pattern State. Nous avons fait ce choix car il est adapté à un système de combat tour par tour. En effet, ce design pattern nous permet de décomposer le système de combat en de différents états indépendants les un les autres permettant de contrôler les actions du joueur et événements selon le state courant. 
Nous avons également essayé d’implémenter le pattern MVC, nous avons un Fighter Controller lié à un Template et un Fighter (Modèle). Ils sont initialisés au lancement du combat. Grâce à l’héritage, nous avons dissocié personnages joueurs et monstres : ainsi, seuls les personnages joueurs peuvent gagner de l’expérience, par exemple.
Nous avons opté pour des scènes isométriques pour permettre une meilleure interaction, en raison de notre choix de positionner les différents acteurs sur un quadrillage.


#### Pathfinding :
Le combat se déroule sur un damier. Chaque case est un gameObject, et les personnages se déplacent de case en case. Pour le déplacement du personnage, nous avons intégré un système de Pathfinding afin de trouver automatiquement le chemin le plus court pour atteindre la case souhaitée malgré les cases bloquantes. Nous avons donc implémenté le système A* de pathfinding. Nous avons choisi cet algorithme car il est optimisé pour les directions uniques, ce qui est notre cas. En effet, il priorise une recherche directe vers la destination.
Pour la portée, nous mesurons simplement le nombre de cases entre le personnage et sa cible, sans prendre en compte les cases bloquantes. Les ennemis prennent la distance en compte pour leurs attaques.


# Personnages et ennemis :

En début de jeu, trois classes sont sélectionnables, chaque classe possède des statistiques différentes et des compétences uniques. Le personnage est accompagné de deux alliés jouables en combat avec leurs propres statistiques et compétences.
Les ennemis possèdent eux aussi leurs propres classes avec leurs compétences. La gestion des mouvements et compétences est gérée dans le design pattern State.

Chaque classe compte un total de six statistiques qui sont les suivantes : vie, attaque, défense, initiative, points d’action et points de mouvement. Quand un personnage n’a plus de vie, il ne peut plus se battre.
Lors d’une attaque, on retranche (attaque - défense/2) à la vie. Si la cible se défend, elle applique toute sa défense.
Les points de mouvement limitent le déplacement possible à chaque tour (un point est dépensé à chaque case traversée).
Les points d’action et l’initiative n’ont pas eu le temps d’être implémentés.


# Les objets :

Un système d’objets est intégré au jeu avec des consommables, et des équipements. Le joueur est amené à croiser sur sa route ces deux types d’objets qu’il peut ramasser. Il a la possibilité d’utiliser des consommables afin de remonter ses statistiques, exemple : une potion de soin peut être consommée pour récupérer des points de vie. Quant aux équipements, ils peuvent être portés afin d’augmenter de manière permanente (jusqu’à qu’ils soient déséquipés), les statistiques de n’importe quel personnage. Chaque objet est représenté par une image, et possède une liste de modificateurs, qui sont les valeurs des statistiques sur lesquelles l’objet influe.

#### L’architecture :
Tous les objets héritent d’une classe mère qui elle-même hérite de Scriptable Objects, car on souhaite persister toutes les informations relatives à ceux-ci. Un objet que le joueur peut ramasser est une instance d’une classe spéciale qui hérite de MonoBehaviour. En effet, nous avons utilisé le principe des collisions d’Unity pour permettre la collection des objets.



# L’inventaire :

Le joueur possède un inventaire dans lequel il peut ranger une quantité limitée d’objets. Il est accessible à tout moment depuis le monde principal en appuyant sur la touche “i”. C’est depuis ce menu que le joueur à la possibilité d’utiliser un consommable.

#### L’architecture :
Le système d’inventaire est structuré en suivant les principes du motif de conception MVC. Le contrôleur se charge d’instancier le modèle et la vue. Ces derniers communiquent avec le contrôleur par le biais d'événements.



# L’équipement :

Comme expliqué précédemment, le joueur peut utiliser des objets équipables sur ses personnages. Ces équipements sont classés par type qui sont au nombre de quatre : casque, torse, gants et arme. Au sein du monde principal, le joueur peut utiliser un équipement depuis l’inventaire à l’aide d’un clique droit. La liste des objets actuellement équipés apparaît sur le panneau à gauche de l’inventaire. Chaque personnage peut porter des équipements. Cependant un seul panneau est affiché à la fois, des onglets sont à disposition pour changer l’équipement affiché, c’est celui qui est affiché qui accueillera les équipements sur lesquels l’utilisateur effectuera un clique droit. C’est en faisant la même opération depuis ce menu que l’on peut retirer un équipement d’un personnage. Dans ce même menu sont affichés le nom du personnage, ainsi que ses statistiques qui évoluent en fonction des objets équipés.

#### L’architecture :
Le système d’équipements utilise le motif de conception MVC. Pour chaque personnage créé dans le jeu, un contrôleur est instancié, qui va lui-même instancier la vue et le modèle correspondant au personnage. Des événements leur permettent de communiquer avec le contrôleur associé.


# Rôle de chaque membre :

blanch_b: Il s'est occupé du graphique et des animations des personnages ainsi que les tilemaps.
baudet_c: Il s'est occupé de l'inventaire et des équipements.
la_t: Il s'est occupé du système de combat, le système de tilemap, le pathfinding et les outils internes.
maro_j: Il s'est occupé avec la_t pour le système de combat, portée des attaques, le déplacement et attaque des monstres.


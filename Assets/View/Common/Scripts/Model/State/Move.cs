﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Assets.View.Common.Scripts.Controller.Characters.Players;
using Assets.View.Common.Scripts.Template.Character;
using Assets.View.Common.Scripts.Model.Map;
using Assets.View.Common.Scripts.Model.Fighter;
using UnityEngine;
using UnityEngine.Tilemaps;

namespace Assets.View.Common.Scripts.Model.State
{
    public class Move:State
    {
        private List<TileGo> path;

        public override IEnumerator Start()
        {
            yield break;
        }

        public override IEnumerator MovePlayer()
        {
            var pathFinder = new PathFinder();
            Fighter.Fighter player = BattleEngine.Instance.GetCurrentPlayerController().GetFighter();
            BattleEngine.Instance.GetCurrentPlayerController().GetFighterTemplate().PlayAnimator("WALK");
            path = pathFinder.FindPath(player.GetStandingOnTile(), player.GetDestinationTile());
            if (path.Count > 0)
            {
                MoveAlongPath();
            } else
            {
                BattleEngine.Instance.SetState(new CameraMove());
            }
            yield break;
        }

        private void MoveAlongPath()
        {
            PlayerController pc = (PlayerController) BattleEngine.Instance.GetCurrentPlayerController();
            FighterTemplate playerTemplate = pc.GetFighterTemplate();
            float speed = 4f;
            float step = speed * Time.deltaTime;

            float zIndex = path[0].transform.position.z;
          
            playerTemplate.transform.position = Vector2.MoveTowards(playerTemplate.transform.position, path[0].transform.position, step);
            playerTemplate.transform.position = new Vector3(playerTemplate.transform.position.x, playerTemplate.transform.position.y, zIndex);

            if (Vector2.Distance(playerTemplate.transform.position, path[0].transform.position) < 0.00001f)
            {
                PositionCharacterOnLine(path[0]);
                path.RemoveAt(0);
                pc.DecrementMovementPoints();
            }

            if (path.Count == 0 || !pc.CanMove())
            {
                BattleEngine.Instance.SetState(new CameraMove());
            }
        }
        private void PositionCharacterOnLine(TileGo tile)
        {
            PlayerController pController = (PlayerController) BattleEngine.Instance.GetCurrentPlayerController();
            pController.GetFighterTemplate().transform.position = new Vector3(tile.transform.position.x, tile.transform.position.y + 0.0001f, tile.transform.position.z);
            pController.GetFighterTemplate().GetSprite().sortingOrder = tile.GetComponent<SpriteRenderer>().sortingOrder;
            pController.SetStandingOnTile(tile);
        }


    }
}

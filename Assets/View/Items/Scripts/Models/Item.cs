using System.Collections.Generic;
using UnityEngine;
using Assets.View.Items.Scripts.Enums;


namespace Assets.View.Items.Scripts.Models
{
    public abstract class Item : ScriptableObject
    {
        #region Serialized Fields

        [SerializeField] private EItemType _type;
        [SerializeField] private int _maxStackSize;
        [SerializeField] private Sprite _icon;
        [SerializeField] private List<ModifierData> _modifiersData = new List<ModifierData>();

        #endregion

        #region Getters

        public int GetId() => GetInstanceID();

        public EItemType GetEType() => _type;

        public int GetMaxStackSize() => _maxStackSize;

        public Sprite GetIcon() => _icon;

        public List<ModifierData> GetModifiers() => _modifiersData;

        #endregion

        #region Setters

        public void SetType(EItemType pType)
        {
            _type = pType;
        }

        public void SetMaxStackSize(int pMaxStackSize)
        {
            _maxStackSize = pMaxStackSize;
        }

        public void SetIcon(Sprite pIcon)
        {
            _icon = pIcon;
        }

        #endregion

        #region Public Methods

        // TODO bonus : Drop()

        #endregion
    }
}

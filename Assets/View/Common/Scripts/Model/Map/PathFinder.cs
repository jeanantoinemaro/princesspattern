﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

namespace Assets.View.Common.Scripts.Model.Map
{
    public class PathFinder
    {
        public List<TileGo> FindPath(TileGo start, TileGo end)
        {
            List<TileGo> openList = new List<TileGo>();
            List<TileGo> closedList = new List<TileGo>();

            openList.Add(start);
            while (openList.Count > 0)
            {
                TileGo currentOverlayTile = openList.OrderBy(x => x.F).First();

                openList.Remove(currentOverlayTile);
                closedList.Add(currentOverlayTile);

                if (currentOverlayTile == end)
                {
                    
                    return GetFinishedList(start, end);
                }

                foreach (var tile in GetNeightbourOverlayTiles(currentOverlayTile))
                {
                    if (tile.isBlocked || closedList.Contains(tile) || Mathf.Abs(currentOverlayTile.transform.position.z - tile.transform.position.z) > 1)
                    {
                        continue;
                    }
                    tile.G = GetManhattenDistance(start, tile);
                    tile.H = GetManhattenDistance(end, tile);

                    tile.Previous = currentOverlayTile;


                    if (!openList.Contains(tile))
                    {
                        openList.Add(tile);
                    }
                }
            }

            return new List<TileGo>();
        }

        private List<TileGo> GetFinishedList(TileGo start, TileGo end)
        {
            List<TileGo> finishedList = new List<TileGo>();
            TileGo currentTile = end;

            while (currentTile != start)
            {
                finishedList.Add(currentTile);
                currentTile = currentTile.Previous;
            }
            finishedList.Reverse(); 
            return finishedList;
        }

        public int GetManhattenDistance(TileGo start, TileGo tile)
        {
            return Mathf.Abs(start.gridLocation.x - tile.gridLocation.x) + Mathf.Abs(start.gridLocation.y - tile.gridLocation.y) + Mathf.Abs(start.gridLocation.z - tile.gridLocation.z); ;
        }

        private List<TileGo> GetNeightbourOverlayTiles(TileGo currentOverlayTile)
        {
            var map = MapEngine.Instance.GetMap();

            List<TileGo> neighbours = new List<TileGo>();

            //right
            Vector2Int locationToCheck = new Vector2Int(
                currentOverlayTile.gridLocation.x + 1,
                currentOverlayTile.gridLocation.y
            );

            if (map.ContainsKey(locationToCheck))
            {
                neighbours.Add(map[locationToCheck]);
            }

            //left
            locationToCheck = new Vector2Int(
                currentOverlayTile.gridLocation.x - 1,
                currentOverlayTile.gridLocation.y
            );

            if (map.ContainsKey(locationToCheck))
            {
                neighbours.Add(map[locationToCheck]);
            }

            //top
            locationToCheck = new Vector2Int(
                currentOverlayTile.gridLocation.x,
                currentOverlayTile.gridLocation.y + 1
            );

            if (map.ContainsKey(locationToCheck))
            {
                neighbours.Add(map[locationToCheck]);
            }

            //bottom
            locationToCheck = new Vector2Int(
                currentOverlayTile.gridLocation.x,
                currentOverlayTile.gridLocation.y - 1
            );

            if (map.ContainsKey(locationToCheck))
            {
                neighbours.Add(map[locationToCheck]);
            }

            return neighbours;
        }

        public int FindStraightDistance(TileGo start, TileGo end)
        {
            int distance = GetManhattenDistance(start, end);
            return distance;
        }

    }
}

using UnityEditor.UIElements;
using UnityEngine;

namespace Assets.View.Common.Scripts.Controller.Camera
{
    public class CameraController : MonoBehaviour
    {
        [SerializeField] private Transform _target;
        [SerializeField] private float _speed;
        private float _desiredDuration = 3f;
        private float _elapsedTime;
        private Vector3 v;
        [SerializeField] private Vector3 _offset;
        // Update is called once per frame
        void Start()
        {
            v = Vector3.zero;
        }
        void FixedUpdate()
        {
            _elapsedTime += Time.deltaTime;
            transform.position = Vector3.SmoothDamp(transform.position, _target.position, ref v, 4f);
            transform.position = new Vector3(transform.position.x, transform.position.y, -8);
            //transform.Translate(_target.position * _speed * Time.deltaTime);

        }
    }
}

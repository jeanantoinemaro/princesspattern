using Assets.View.Common.Scripts.Model.Fighter.Player;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UI_EquipmentsButtonsPanel : MonoBehaviour
{
    #region Serialized Fields

    [SerializeField] private RectTransform _buttonsPanel;
    [SerializeField] private UI_EquipmentsButton _button;

    #endregion

    #region Private Fields

    private List<UI_EquipmentsButton> _buttons = new List<UI_EquipmentsButton>();
    private List<GameObject> _equipmentsContentPanels = new List<GameObject>();
    private List<PlayerFighter> _players = new List<PlayerFighter>(); // in case the players are renaming
    private int _currentTab; // keep track of the tab that is currently visible for the next time inventory is opened

    #endregion

    #region Private Methods

    private void HandleButtonClick(UI_EquipmentsButton pEquipmentsButton)
    {
        ShowTab(_buttons.IndexOf(pEquipmentsButton));
    }

    #endregion

    #region Public Methods

    public void InitializeUI()
    {
        UI_EquipmentsButton instanciatedButton;

        for (int i = 0; i < _equipmentsContentPanels.Count; i++)
        {
            instanciatedButton = Instantiate(_button, _buttonsPanel);
            _buttons.Add(instanciatedButton);
            instanciatedButton.SetData(_players[i].GetName());
            instanciatedButton.OnButtonClicked += HandleButtonClick;
        }
        ShowTab(_currentTab = 0);
    }

    public void AddEquipmentsContent(GameObject pEquipmentsContent, PlayerFighter pPlayer)
    {
        _equipmentsContentPanels.Add(pEquipmentsContent);
        _players.Add(pPlayer);
    }

    public int GetActiveEquipmentsContentPanel()
    {
        for (int i = 0; i < _equipmentsContentPanels.Count; i++)
        {
            if (_equipmentsContentPanels[i].activeInHierarchy)
                return i;
        }
        return 0;
    }

    public void HideAllTabs()
    {
        for (int i = 0; i < _equipmentsContentPanels.Count; i++)
        {
            _equipmentsContentPanels[i].SetActive(false);
            if (ColorUtility.TryParseHtmlString("#8C816B", out Color color))
            {
                _buttons[i].GetComponent<Image>().color = color;
            }
        }
    }

    public void ShowTab(int pIndex)
    {
        HideAllTabs();

        if (ColorUtility.TryParseHtmlString("#CBBC9C", out Color color))
        {
            _buttons[pIndex].GetComponent<Image>().color = color;
        }
        _currentTab = pIndex;

        _equipmentsContentPanels[pIndex].SetActive(true);
    }

    public void OnEnable() // on inventory opening we show the tab that was visible on closing, default is the first
    {
        ShowTab(_currentTab);
    }

    #endregion
}

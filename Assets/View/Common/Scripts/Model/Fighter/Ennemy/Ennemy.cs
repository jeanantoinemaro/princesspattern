﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Assets.View.Common.Scripts.ScriptableObjects;
using Assets.View.Common.Scripts.ScriptableObjects.AdvancedCharacter;
using JetBrains.Annotations;

namespace Assets.View.Common.Scripts.Model.Fighter.Ennemy
{
    public class Ennemy : Fighter
    {
        public Ennemy(string name, AdvancedCharacter character) : base(name, character)
        {

        }

    }
}

﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Assets.View.Common.Scripts.Controller.Characters.Players;
using Assets.View.Common.Scripts.Template.Character;
using UnityEngine;

namespace Assets.View.Common.Scripts.Model.Util
{
    public static class SaveWorldPlayer
    {
        private static string _basePath = Application.persistentDataPath + "/data/";
        public static void Save(Player.Player player)
        {
            if (!Directory.Exists(_basePath))
            {
                Directory.CreateDirectory(_basePath);
            }
            WorldEngine.PlayerWorldData data = new WorldEngine.PlayerWorldData(player);
            string jsonData = JsonUtility.ToJson(data);
            File.WriteAllText(_basePath+"player.json", jsonData);

            Save();
        }

        public static void Save()
        {
            if (!Directory.Exists(_basePath))
            {
                Directory.CreateDirectory(_basePath);
            }
            foreach (PlayerController pc in GameEngine.Instance.GetPlayerControllers())
            {
                FighterTemplate template = pc.GetFighterTemplate();
                PlayerData data = new PlayerData(pc.GetPlayer(), pc.GetEquipmentsController().GetEquipmentsModel().GetCurrentEquipments());
                string path = _basePath + template.GetName() + ".json";
                string jsonData = JsonUtility.ToJson(data);
                File.WriteAllText(path, jsonData);
            }
        }

        public static WorldEngine.PlayerWorldData Load()
        {
            string jsonData = File.ReadAllText(_basePath + "player.json");
            WorldEngine.PlayerWorldData data = JsonUtility.FromJson<WorldEngine.PlayerWorldData>(jsonData);
            return data;
        }

        public static void DeleteSave()
        {
            File.Delete(_basePath + "player.json");
        }
    }
}

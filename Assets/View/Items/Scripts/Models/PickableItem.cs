using System.Collections;
using Assets.View.Common.Scripts;
using UnityEngine;

namespace Assets.View.Items.Scripts.Models
{
    public class PickableItem : MonoBehaviour
    {
        #region Serialized Fields

        [SerializeField] private Item _inventoryItem;
        [SerializeField] private int _quantity = 1;
        //[SerializeField] private AudioSource _audioSource; // TODO if we have time
        [SerializeField] private float _duration = 0.0f; // pick up animation duration

        #endregion

        #region Getters

        public Item GetItem() => _inventoryItem;

        public int GetQuantity() => _quantity;

        #endregion

        #region Setters

        public void SetQuantity(int pQuantity)
        {
            _quantity = pQuantity;
        }

        #endregion

        #region Unity Methods

        private void Start()
        {
            GetComponent<SpriteRenderer>().sprite = _inventoryItem.GetIcon();
        }

        #endregion

        #region Private Methods

        private IEnumerator AnimateItemPickup()
        {
            // TODO
            float currentTime = 0;
            while (currentTime < _duration)
            {
                currentTime += Time.deltaTime;
                yield return null;
            }
            Destroy(gameObject);
        }

        #endregion

        #region Public Methods

        public void DestroyItem()
        {
            //StartCoroutine(AnimateItemPickup());
            gameObject.SetActive(false);
            GameEngine.Instance.GetItemPrefabs()[WorldEngine.Instance.GetItemsClone().IndexOf(gameObject)].SetActive(false);
        }

        #endregion
    }
}
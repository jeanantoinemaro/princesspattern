using UnityEngine;

namespace Assets
{
    public class MoveCamera : MonoBehaviour
    {
        #region Serialized Fields
        [SerializeField]
        private float _speed;
        #endregion

        void Update()
        {

            Vector2 dir = Vector2.zero;

            if (Input.GetKey(KeyCode.Z) && transform.position.y < 6)
            {
                dir.y = 1;
            }
            if (Input.GetKey(KeyCode.Q) && transform.position.x > -2)
            {
                dir.x = -1;
            }
            if (Input.GetKey(KeyCode.S) && transform.position.y > 0)
            {
                dir.y = -1;
            }
            if (Input.GetKey(KeyCode.D) && transform.position.x < 2)
            {
                dir.x = 1;
            }

            dir.Normalize();

            GetComponent<Rigidbody2D>().velocity = _speed * dir;


        }
    }
}

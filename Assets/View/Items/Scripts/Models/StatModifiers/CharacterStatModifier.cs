using Assets.View.Common.Scripts.Model.Fighter.Player;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Assets.View.Items.Scripts.Models.StatModifiers
{
    public abstract class CharacterStatModifier : ScriptableObject
    {
        public abstract void EquipableAffectCharacter(PlayerFighter pPlayer, float pValue);
        public abstract void ConsumableAffectCharacter(PlayerFighter pPlayer, float pValue);
    }
}
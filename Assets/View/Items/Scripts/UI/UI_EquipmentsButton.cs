using System;
using TMPro;
using UnityEngine;
using UnityEngine.EventSystems;

public class UI_EquipmentsButton : MonoBehaviour
{
    #region Serialized Fields

    [SerializeField] private TMP_Text _text;

    #endregion

    #region Fields

    public event Action<UI_EquipmentsButton> OnButtonClicked;

    #endregion

    #region Public Methods

    public void SetData(string pText)
    {
        _text.text = pText;
    }

    public void OnPointerClick(BaseEventData pData)
    {
        PointerEventData pointerData = (PointerEventData)pData;
        if (pointerData.button == PointerEventData.InputButton.Left)
        {
            OnButtonClicked?.Invoke(this);
        }
    }

    #endregion
}

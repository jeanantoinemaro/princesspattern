using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Serialization.Formatters.Binary;
using Assets.View.Common.Scripts.Controller.Characters;
using Assets.View.Common.Scripts.Controller.Characters.Ennemy;
using Assets.View.Common.Scripts.Controller.Characters.Players;
using Assets.View.Common.Scripts.Enum;
using Assets.View.Common.Scripts.Model;
using Assets.View.Common.Scripts.Model.Fighter;
using Assets.View.Common.Scripts.Model.Fighter.Ennemy;
using Assets.View.Common.Scripts.Model.Map;
using Assets.View.Common.Scripts.Model.State;
using Assets.View.Common.Scripts.Model.Util;
using Assets.View.Common.Scripts.ScriptableObjects.AdvancedCharacter;
using Assets.View.Common.Scripts.Template.Character;
using Assets.View.Common.Scripts.Template.Ennemy;
using JetBrains.Annotations;
using UnityEngine;
using Assets.View.Items.Scripts.Models;
using Assets.View.Items.Scripts.Controllers;
using Action = Assets.View.Common.Scripts.ScriptableObjects.BattleAction;
using PlayerFighter = Assets.View.Common.Scripts.Model.Fighter.Player.PlayerFighter;

namespace Assets.View.Common.Scripts
{
    public class BattleEngine : StateMachine
    {
        [SerializeField] private Canvas _canvas;
        [SerializeField] private int _playersNumber;
        [SerializeField] private List<PlayerController> _playerControllers;
        [SerializeField] private List<GameObject> _playerPrefabs;
        [SerializeField] private List<GameObject> _ennemyPrefabs;
        private List<EnnemyController> _ennemyControllers;
        [SerializeField] private string _associatedWorldScene;

        private FighterController _currentPlayerController;
        public static BattleEngine Instance;

        [SerializeField] private GameObject _movesContainer;
        [SerializeField] private GameObject _actions;

        [SerializeField] private BattleUI _battleUI;
        public BattleUI BattleUI => _battleUI;

        #region Unity Methods
        void Start()
        {
             GameEngine.Instance.InventoryInit();
            _playerPrefabs = GameEngine.Instance.GetPlayerBattlePrefabs();
            _playerControllers = new List<PlayerController>();
            foreach (PlayerController pc in GameEngine.Instance.GetPlayerControllers())
            {
                pc.Selected += SetSelectedController;
            }
            if (_ennemyControllers == null)
            {
                _ennemyControllers = new List<EnnemyController>();
            }
            _playersNumber = _playerPrefabs.Count;
            SetState(new DispatchPlayers());
        }

        private void Awake()
        {
            if (Instance == null)
            {
                Instance = this;
                //DontDestroyOnLoad(this);
            }
            else
            {
                Destroy(this);
            }
            _ennemyControllers = new List<EnnemyController>();
        }
        #endregion

        #region PUBLIC METHODS

        public bool HaveAllPlayersActed()
        {
            foreach (PlayerController playerController in _playerControllers)
            {
                if (playerController.IsReady())
                    return false;
            }
            return true;
        }
        public bool AreAllPlayersDead()
        {
            foreach (PlayerController playerController in _playerControllers)
            {
                if (!playerController.IsDead())
                {
                    return false;
                }
            }
            return true;
        }

        public void NewPlayerTurn()
        {
            foreach (PlayerController playerController in _playerControllers)
            {
                SetNewTurn(playerController);
            }

        }

        private void SetNewTurn(FighterController fController)
        {
            fController.GetFighter().SetIsDefending(false);
            fController.SetIsReady(true);
            fController.GetFighter().GetCharacter().ResetActionPoints();
            fController.ResetMovementPoints();
        }

        public bool HaveAllEnemiesActed()
        {
            foreach (EnnemyController eController in _ennemyControllers)
            {
                if (eController.IsReady())
                    return false;
            }
            return true;
        }

        public bool AreAllEnemiesDead()
        {
            foreach (EnnemyController eController in _ennemyControllers)
            {
                if (!eController.IsDead())
                {
                    return false;
                }
            }
            return true;
        }

        public void NewEnemyTurn()
        {
            foreach (EnnemyController eController in _ennemyControllers)
            {
                SetNewTurn(eController);
            }
        }

        public PlayerController SpawnOnTile(GameObject playerPrefab, TileGo tile)
        {
            GameObject playerGo = Instantiate(playerPrefab, transform);
            playerGo.SetActive(true);
            playerGo.transform.position = tile.transform.position;

            _playersNumber--;
            return GameEngine.Instance.AddPlayerController(playerGo);
        }

        public EnnemyController AddEnnemyController(Ennemy enemy, FighterTemplate fTemplate)
        {
            EnnemyController eController = new EnnemyController(enemy, fTemplate);
            eController.GetFighterTemplate().Selected += eController.OnSelected;
            eController.Selected += SetSelectedController;
            if (_ennemyControllers == null)
            {
                _ennemyControllers = new List<EnnemyController>();
            }
            _ennemyControllers.Add(eController);
            return eController;

        }

        public void InitBattleHP()
        {
            foreach (EnnemyController eController in _ennemyControllers)
            {
                eController.GetFighter().GetCharacter().Heal(0);
            }
            foreach (PlayerController pController in _playerControllers)
            {
                pController.GetFighter().GetCharacter().Heal(0);
            }

            
        }
        #endregion

        #region GETTERS

        public FighterController GetCurrentPlayerController()
        {
            return _currentPlayerController;
        }

        public List<PlayerController> GetPlayerControllers()
        {
            return _playerControllers;
        }
        public List<EnnemyController> GetEnnemyControllers()
        {
            return _ennemyControllers;
        }
        public int GetPlayersNumber()
        {
            return _playersNumber;
        }

        public Canvas GetCanvas()
        {
            return _canvas;
        }

        public GameObject GetMoveContainer()
        {
            return _movesContainer;
        }

        public GameObject GetActions()
        {
            return _actions;
        }

        public List<GameObject> GetPlayerPrefabs()
        {
            return _playerPrefabs;
        }

        #endregion

        #region SETTERS

        public void SetCurrentPlayerController(FighterController fController)
        {
            _currentPlayerController = fController;
        }

        public void SetSelectedController(FighterController fController)
        {
            StartCoroutine(_state.SelectController(fController));
        }

        public List<GameObject> GetEnnemmyPrefabs()
        {
            return _ennemyPrefabs;
        }

        public string GetAssociatedWorldScene()
        {
            return _associatedWorldScene;
        }

        #endregion
    }

    [Serializable]
    public class PlayerData
    {
        public int _life;
        public int _maxLife;
        public int _exp;
        public int _maxExp;
        public int _level;
        public int _attack;
        public int _defense;
        public List<string> _equipments;
        public Dictionary<string, int> _items;

        public PlayerData(PlayerFighter player, List<EquipmentItem> equipments)
        {
            _life = player.GetCharacter().GetLife();
            _maxLife = player.GetCharacter().GetMaxLife();
            _exp = player.GetExp();
            _level = player.getLevel();
            _attack = player.GetCharacter().GetAttack();
            _defense = player.GetCharacter().GetDefense();
            _maxExp = player.GetMaxExperience();
            _equipments = new List<string>();
            _items = new Dictionary<string, int>();
            foreach (EquipmentItem item in equipments)
            {
                _equipments.Add(item.name);
            }

        }
    }


}

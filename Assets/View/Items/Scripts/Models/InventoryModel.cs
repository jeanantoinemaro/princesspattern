using System.Collections.Generic;
using UnityEngine;
using System;
using System.Linq;

namespace Assets.View.Items.Scripts.Models
{
    [CreateAssetMenu]
    public class InventoryModel : ScriptableObject // scriptable car on peut cr�er des inventaires pour shops, trade, tutoriels...
    {
        #region Serialized Fields

        [SerializeField] private List<InventoryItem> _inventoryItems = new List<InventoryItem>(); // for storing items in inventory
        [SerializeField] private int _size;

        #endregion

        #region Events

        public event Action<Dictionary<int, InventoryItem>> OnInventoryUpdated;

        #endregion

        #region Getters

        public int GetSize() => _size;

        #endregion

        #region Public Methods

        public int AddItem(Item pItem, int pQuantity)
        {
            if (pItem.GetMaxStackSize() == 1)
            {
                foreach (InventoryItem item in _inventoryItems)
                {
                    while (pQuantity > 0 && !IsInventoryFull())
                    {
                        pQuantity -= AddItemToFirstFreeSlot(pItem, 1);
                    }
                    InformAboutChange();
                    return pQuantity;
                }
            }
            pQuantity = AddStackableItem(pItem, pQuantity);
            InformAboutChange();
            return pQuantity;
        }

        public void AddItem(InventoryItem item)
        {
            AddItem(item.item, item.quantity);
        }

        private int AddItemToFirstFreeSlot(Item pItem, int pQuantity)
        {
            InventoryItem newItem = new InventoryItem
            {
                item = pItem,
                quantity = pQuantity
            };

            for (int i = 0; i < _inventoryItems.Count; i++)
            {
                if (_inventoryItems[i].IsEmpty)
                {
                    _inventoryItems[i] = newItem;
                    return pQuantity;
                }
            }
            return 0;
        }

        private int AddStackableItem(Item pItem, int pQuantity)
        {
            for (int i = 0; i < _inventoryItems.Count; i++)
            {
                if (_inventoryItems[i].IsEmpty)
                    continue;
                if (_inventoryItems[i].item.GetId() == pItem.GetId())
                {
                    int amountPossibleToTake =
                        _inventoryItems[i].item.GetMaxStackSize() - _inventoryItems[i].quantity;

                    if (pQuantity > amountPossibleToTake)
                    {
                        _inventoryItems[i] =
                            _inventoryItems[i].ChangeQuantity(_inventoryItems[i].item.GetMaxStackSize());
                        pQuantity -= amountPossibleToTake;
                    }
                    else
                    {
                        _inventoryItems[i] = _inventoryItems[i]
                            .ChangeQuantity(_inventoryItems[i].quantity + pQuantity);
                        InformAboutChange();
                        return 0;
                    }
                }
            }
            while (pQuantity > 0 && !IsInventoryFull())
            {
                int newQuantity = Mathf.Clamp(pQuantity, 0, pItem.GetMaxStackSize());
                pQuantity -= newQuantity;
                AddItemToFirstFreeSlot(pItem, newQuantity);
            }
            return pQuantity;
        }

        public InventoryItem GetItemAt(int itemIndex)
        {
            return _inventoryItems[itemIndex];
        }

        public void RemoveItem(int pItemIndex, int pAmount)
        {
            if (_inventoryItems.Count > pItemIndex)
            {
                if (_inventoryItems[pItemIndex].IsEmpty)
                    return;
                int reminder = _inventoryItems[pItemIndex].quantity - pAmount;
                if (reminder <= 0)
                    _inventoryItems[pItemIndex] = InventoryItem.GetEmptyItem();
                else
                    _inventoryItems[pItemIndex] = _inventoryItems[pItemIndex].ChangeQuantity(reminder);
                InformAboutChange();
            }
        }

        public Dictionary<int, InventoryItem> GetCurrentInventoryState()
        {
            Dictionary<int, InventoryItem> returnValue = new Dictionary<int, InventoryItem>();
            for (int i = 0; i < _inventoryItems.Count; i++)
            {
                if (_inventoryItems[i].IsEmpty)
                    continue;
                returnValue[i] = _inventoryItems[i];
            }
            return returnValue;
        }

        public void AddItemToSlot(int index, InventoryItem item)
        {
            _inventoryItems[index] = item;
        }

        private bool IsInventoryFull()
            => _inventoryItems.Where(item => item.IsEmpty).Any() == false;

        private void InformAboutChange()
        {
            OnInventoryUpdated?.Invoke(GetCurrentInventoryState());
        }

        #endregion
    }
}

namespace Assets.View.Items.Scripts.Models
{
    // r�cup�rer la valeur d'un item sous forme de structure fait un appel au Stack seulement et non au Heap
    // prot�ge davantage l'inventaire si jamais d'autres classes r�cup�rent sa r�f�rence
    [Serializable]
    public struct InventoryItem
    {
        #region Public Fields

        public int quantity;
        public Item item;

        #endregion

        #region Public Methods

        public bool IsEmpty => item == null;

        public InventoryItem ChangeQuantity(int pNewQuantity)
        {
            return new InventoryItem
            {
                item = item,
                quantity = pNewQuantity
            };
        }
        public static InventoryItem GetEmptyItem()
        {
            return new InventoryItem
            {
                item = null,
                quantity = 0
            };
        }

        #endregion
    }
}

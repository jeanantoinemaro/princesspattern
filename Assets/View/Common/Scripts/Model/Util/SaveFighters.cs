﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Assets.View.Common.Scripts.Controller.Characters.Players;
using Assets.View.Common.Scripts.ScriptableObjects.AdvancedCharacter;
using Assets.View.Common.Scripts.Template.Character;
using Assets.View.Items.Scripts.Models;
using UnityEngine;

namespace Assets.View.Common.Scripts.Model.Util
{
    public static class SaveFighters
    {
        private static string _basePath = Application.persistentDataPath + "/data/";
        private static Dictionary<string, Item> mapItems;
        public static void Save()
        {
            if (!Directory.Exists(_basePath))
            {
                Directory.CreateDirectory(_basePath);
            }
            foreach (PlayerController pc in BattleEngine.Instance.GetPlayerControllers())
            {
                FighterTemplate template = pc.GetFighterTemplate();
                PlayerData data = new PlayerData(pc.GetPlayer(), pc.GetEquipmentsController().GetEquipmentsModel().GetCurrentEquipments());
                string path = _basePath + template.GetName() + ".json";
                string jsonData = JsonUtility.ToJson(data);
                File.WriteAllText(path, jsonData);
            }
        }

        public static Fighter.Player.PlayerFighter Load(string advancedCharacterName, string templateName)
        {
            AdvancedCharacter advancedCharacter = GameEngine.Instance.GetCharacter(advancedCharacterName);
            string path = _basePath + templateName + ".json";
            string jsonData = File.ReadAllText(path);
            PlayerData data = JsonUtility.FromJson<PlayerData>(jsonData);
            advancedCharacter.SetLife(data._life);
            advancedCharacter.SetMaxLife(data._maxLife);
            advancedCharacter.SetAttack(data._attack);
            advancedCharacter.SetDefense(data._defense);
            Fighter.Player.PlayerFighter player = new Fighter.Player.PlayerFighter(templateName, advancedCharacter);
            player.SetExp(data._exp);
            player.SetLevel(data._level);
            player.SetMaxExperience(data._maxExp);
            
            return player;
        }
        public static List<EquipmentItem> LoadEquipment(string templateName)
        {
            string path = _basePath + templateName + ".json";
            string jsonData = File.ReadAllText(path);
            PlayerData data = JsonUtility.FromJson<PlayerData>(jsonData);

            if (null == mapItems)
            {
                mapItems = new Dictionary<string, Item>();
                foreach (Item item in GameEngine.Instance.GetListItems())
                {
                    mapItems.Add(item.name, item);
                }
            }

            List<EquipmentItem> equipments = new List<EquipmentItem>();

            foreach (string eName in data._equipments)
            {
                if (eName == "") continue;
                equipments.Add( (EquipmentItem) mapItems[eName]);
            }
            return equipments;
        }

        public static void DeleteAllFighters()
        {
            if (Directory.Exists(_basePath))
            {
                Directory.Delete(_basePath, true);
            }
        }

    }
}

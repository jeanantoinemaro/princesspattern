using TMPro;
using UnityEngine;

namespace Assets.View.Items.Scripts.UI
{
    public class UI_EquipmentsPanelTitle : MonoBehaviour
    {
        #region Serialized Fields

        [SerializeField] private TMP_Text _title;

        #endregion

        #region Setters

        public void SetTitle(string pTitle)
        {
            _title.text = pTitle;
        }

        #endregion
    }
}

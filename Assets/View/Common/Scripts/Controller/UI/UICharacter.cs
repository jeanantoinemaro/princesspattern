using System;
using Assets.View.Common.Scripts.Controller.Characters.Players;
using Assets.View.Common.Scripts.Model.Fighter;
using Assets.View.Common.Scripts.Model.Map;
using Assets.View.Common.Scripts.ScriptableObjects;
using Assets.View.Common.Scripts.Template.Character;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using Action = System.Action;

namespace Assets.View.Common.Scripts.Controller.UI
{
    public class UICharacter : MonoBehaviour, IPointerDownHandler
    {
        // Start is called before the first frame update
        [SerializeField] private bool _selected;
        [SerializeField] private Image _image;
        [SerializeField] private Text _name;
        [SerializeField] private GameObject _playerPrefab;
        public PlayerController _playerController;
        public event Action<UICharacter> OnSelected;


        public void OnPointerDown(PointerEventData eventData)
        {
            SetSelected(true);
            UnityEngine.Cursor.SetCursor(_image.sprite.texture, new Vector2(100, 100), CursorMode.Auto);
        }

        public bool GetSelected()
        {
            return _selected;
        }

        public void SetSelected(bool pSelected)
        {
            _selected = pSelected;
        }

        public void SetPlayerPrefab(GameObject pPlayer)
        {
            _playerPrefab = pPlayer;
        }
        public GameObject GetPlayerPrefab()
        {
            return _playerPrefab;
        }

        public void SetSprite(Sprite pSprite)
        {
            _image.sprite = pSprite;
        }

        public void SetName(string pName)
        {
            _name.text = pName;
        }
    }
}

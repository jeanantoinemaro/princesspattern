using Assets.View.Items.Scripts.Enums;
using System;
using System.Collections.Generic;
using Assets.View.Common.Scripts.ScriptableObjects.AdvancedCharacter;
using UnityEngine;

namespace Assets.View.Items.Scripts.UI
{
    public class UI_StatsMenu : MonoBehaviour
    {
        #region Serialized Fields

        [SerializeField] private UI_StatsItem _statPrefab;
        [SerializeField] private RectTransform _contentPanel;

        #endregion

        #region Private Fields

        private List<UI_StatsItem> _uiStats = new List<UI_StatsItem>();

        #endregion

        #region Public Methods

        public void InitializeStatsUI(AdvancedCharacter pPlayer)
        {
            for (int i = 0; i < Enum.GetNames(typeof(EStats)).Length; i++)
            {
                UI_StatsItem uiItem = Instantiate(_statPrefab, Vector3.zero, Quaternion.identity);
                uiItem.transform.SetParent(_contentPanel);
                _uiStats.Add(uiItem);
            }
            InitializeData(pPlayer);
        }

        public void UpdateData(AdvancedCharacter pAdvancedCharacter)
        {
            _uiStats[0].SetValue(pAdvancedCharacter.GetLife().ToString() + " / " + pAdvancedCharacter.GetMaxLife().ToString());
            _uiStats[1].SetValue(pAdvancedCharacter.GetAttack().ToString());
            _uiStats[2].SetValue(pAdvancedCharacter.GetDefense().ToString());
            _uiStats[3].SetValue(pAdvancedCharacter.GetInitiative().ToString());
            _uiStats[4].SetValue(pAdvancedCharacter.GetActionPoint().ToString() + " / " + pAdvancedCharacter.GetMaxActionPoint().ToString());
            _uiStats[5].SetValue(pAdvancedCharacter.GetMovementPoint().ToString() + " / " + pAdvancedCharacter.GetMaxMovementPoint().ToString());
        }

        #endregion

        #region Private Methods

        private void InitializeData(AdvancedCharacter pAdvancedCharacter)
        {
            _uiStats[0].SetData(EStats.Life.ToString(), pAdvancedCharacter.GetLife().ToString() + " / " + pAdvancedCharacter.GetMaxLife().ToString());
            _uiStats[1].SetData(EStats.Attack.ToString(), pAdvancedCharacter.GetAttack().ToString());
            _uiStats[2].SetData(EStats.Defense.ToString(), pAdvancedCharacter.GetDefense().ToString());
            _uiStats[3].SetData(EStats.Initiative.ToString(), pAdvancedCharacter.GetInitiative().ToString());
            _uiStats[4].SetData(EStats.AP.ToString(), pAdvancedCharacter.GetActionPoint().ToString() + " / " + pAdvancedCharacter.GetMaxActionPoint().ToString());
            _uiStats[5].SetData(EStats.MP.ToString(), pAdvancedCharacter.GetMovementPoint().ToString() + " / " + pAdvancedCharacter.GetMaxMovementPoint().ToString());
        }

        #endregion
    }
}
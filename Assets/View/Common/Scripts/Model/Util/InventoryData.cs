﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Assets.View.Items.Scripts.Models;

namespace Assets.View.Common.Scripts.Model.Util
{
    [Serializable]
    public class InventoryData
    {
        public List<ItemData> _inventory = new List<ItemData>();

        public InventoryData(List<ItemData> inventory)
        {
            _inventory = inventory;
        }
    }
    [Serializable]
    public class ItemData
    {
        public int _position;
        public int _quantity;
        public string _name;

        public ItemData(string name, int quantity, int position)
        {
            _name = name;
            _quantity = quantity;
            _position = position;
        }
    }
}

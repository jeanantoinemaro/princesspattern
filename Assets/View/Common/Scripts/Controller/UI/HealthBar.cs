﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;
using UnityEngine.UI;

namespace Assets.View.Common.Scripts.Controller.UI
{
    public class HealthBar:MonoBehaviour
    {
        #region Serialized Fields

        [SerializeField] private Slider _slider;
        [SerializeField] private Color _low;
        [SerializeField] private Color _high;
        [SerializeField] private Vector3 _offset;
        #endregion

        #region Unity Methods
        public void Update()
        {
            _slider.transform.position =
                UnityEngine.Camera.main.WorldToScreenPoint(transform.parent.position + _offset);
        }
        #endregion

        #region Public Methods

        public void SetHealth(float health, float maxHealth)
        {
            //_slider.gameObject.SetActive(health < maxHealth);
            _slider.value = health;
            _slider.maxValue = maxHealth;

            _slider.fillRect.GetComponentInChildren<Image>().color = Color.Lerp(_low, _high, _slider.normalizedValue);
        }
        #endregion
    }
}

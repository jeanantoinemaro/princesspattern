using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Assets.View.Common.Scripts.Controller.Characters;
using Assets.View.Common.Scripts.Controller.Characters.Players;
using Assets.View.Common.Scripts.Controller.Characters.Ennemy;
using Assets.View.Common.Scripts.Model.Map;
using Assets.View.Common.Scripts.ScriptableObjects;
using UnityEngine;

namespace Assets.View.Common.Scripts.Model.State
{
    public class SelectNextPlayer : State
    {
        public override IEnumerator Start()
        {
            if (BattleEngine.Instance.AreAllPlayersDead())
            {
                BattleEngine.Instance.SetState(new LostState());
                yield break;
            }
            if (BattleEngine.Instance.AreAllEnemiesDead())
            {
                BattleEngine.Instance.SetState(new WonState());
                yield break;
            }

            BattleEngine.Instance.GetActions().SetActive(false);
            Debug.Log("Selecting players");
            BattleEngine.Instance.BattleUI.SetInfoText("Select a character");
            if (BattleEngine.Instance.GetCurrentPlayerController()!=null)
            {
                Fighter.Player.PlayerFighter currentPlayer = BattleEngine.Instance.GetCurrentPlayerController().GetFighter() as Fighter.Player.PlayerFighter;
                int range = 0;
                BattleAction _move = currentPlayer.GetCharacter().GetCurrentMove();
                if (_move)
                {
                    range = _move.GetRange();
                }
                MapEngine.Instance.SetTilesTo(false, Math.Max(currentPlayer.GetCharacter().GetBaseMovementPoint(), range));
            }
            
            if (BattleEngine.Instance.HaveAllPlayersActed())
            {
                BattleEngine.Instance.BattleUI.DisableInfoText();
                BattleEngine.Instance.NewEnemyTurn();
                BattleEngine.Instance.SetState(new EnemyTurn());
            }
            yield break;
        }


        public override IEnumerator SelectController(FighterController fController)
        {
            if (fController.GetType() == typeof(EnnemyController))
            {
                yield break;
            }
            if (fController.IsReady())
            {
                BattleEngine.Instance.SetCurrentPlayerController(fController as PlayerController);
                MapEngine.Instance.SetTilesTo(true, fController.GetFighter().GetCharacter().GetMovementPoint());
                BattleEngine.Instance.BattleUI.DisableInfoText();
                BattleEngine.Instance.SetState(new ChooseDestination());
                yield break;
            }
            BattleEngine.Instance.BattleUI.SetDialogText("This character has already acted");
            yield return new WaitForSeconds(1.5f);
            BattleEngine.Instance.BattleUI.DisableDialogText();
            yield break;
        }

    }
}

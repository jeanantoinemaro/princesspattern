﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Assets.View.Common.Scripts.Controller.Characters;
using Assets.View.Common.Scripts.Controller.Characters.Players;
using Assets.View.Common.Scripts.Model.Map;
using UnityEngine;
using UnityEngine.Tilemaps;

namespace Assets.View.Common.Scripts.Model.State
{
    public abstract class State
    {

        public virtual IEnumerator Start()
        {
            yield break;
        }

        public virtual IEnumerator DispatchPlayer(TileGo tile)
        {
            yield break;
        }

        public virtual IEnumerator SelectPlayerController(PlayerController playerController)
        {
            yield break;
        }
        public virtual IEnumerator MovePlayer()
        {
            yield break;
        }

        public virtual IEnumerator AttackChoice()
        {
            yield break;
        }

        public virtual IEnumerator ChooseMove()
        {
            yield break;
        }

        public virtual IEnumerator TargetChoice()
        {
            yield break;
        }

        public virtual IEnumerator ChooseTile(TileGo tile)
        {
            yield break;
        }
        public virtual IEnumerator End()
        {
            yield break;
        }

        public virtual IEnumerator MoveCamera()
        {
            yield break;
        }

        public virtual IEnumerator SelectController(FighterController fController)
        {
           yield break;
        }

        public virtual void MoveEnnemyTo()
        {
            return;
        }

        public virtual IEnumerator CancelAttack()
        {
            yield break;
        }
    }
}

﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace Assets.View.Common.Scripts.Controller.UI
{
    public class StartScript : MonoBehaviour
    {
        public void Start()
        {
            StartCoroutine(Story());
        }

        public IEnumerator Story()
        {
            yield return new WaitForSeconds(10);
            SceneManager.LoadScene("CharacterSelection");
        }
    }
}

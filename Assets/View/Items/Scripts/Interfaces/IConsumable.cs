﻿using Assets.View.Common.Scripts.Model.Fighter.Player;
using UnityEngine;

namespace Assets.View.Items.Scripts.Interfaces
{
    interface IConsumable
    {
        bool ConsumeAffectCharacter(PlayerFighter pPlayer);
    }
}

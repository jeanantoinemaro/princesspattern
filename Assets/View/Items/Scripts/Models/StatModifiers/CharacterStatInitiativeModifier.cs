using Assets.View.Common.Scripts.Model.Fighter.Player;
using Assets.View.Common.Scripts.ScriptableObjects.AdvancedCharacter;
using UnityEngine;

namespace Assets.View.Items.Scripts.Models.StatModifiers
{
    [CreateAssetMenu(menuName = "Stat Modifier/Initiative Modifier", order = 3)]
    public class CharacterStatInitiativeModifier : CharacterStatModifier
    {
        public override void ConsumableAffectCharacter(PlayerFighter pPlayer, float pValue)
        {
            throw new System.NotImplementedException();
        }

        public override void EquipableAffectCharacter(PlayerFighter pPlayer, float pValue)
        {
            AdvancedCharacter advancedCharacter = pPlayer.GetCharacter();
            if (advancedCharacter != null)
            {
                advancedCharacter.SetInitiative(advancedCharacter.GetInitiative() + (int)pValue);
            }
        }
    }
}
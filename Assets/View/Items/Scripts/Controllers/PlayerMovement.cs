
using Assets.View.Common.Scripts;
using Assets.View.Items.Scripts.UI;
using UnityEngine;

namespace Assets.View.Items.Scripts.Controllers
{
    public class PlayerMovement : MonoBehaviour
    {
        #region Serialized Fields
        [SerializeField]
        private float _speed;

        private Animator _animator;
        private SpriteRenderer _sprite;
        #endregion

        #region Private Fields

        private UI_InventoryMenu _inventoryMenu;

        #endregion

        #region Setters

        public void SetInventoryMenu(UI_InventoryMenu pInventoryMenu)
            => _inventoryMenu = pInventoryMenu;

        #endregion

        #region Unity Methods
        // Start is called before the first frame update
        private void Start()
        {
            _animator = GetComponent<Animator>();
            _inventoryMenu = GameEngine.Instance.GetInventoryController().GetInventoryMenu();
            _sprite = GetComponent<SpriteRenderer>();
        }

        // Update is called once per frame
        private void Update()
        {
            Vector2 dir = Vector2.zero;

            if (Input.GetKey(KeyCode.Z))
            {
                dir.y = 1;
            }
            if (Input.GetKey(KeyCode.Q))
            {
                dir.x = -1;
                _sprite.flipX = true;
            }
            if (Input.GetKey(KeyCode.S))
            {
                dir.y = -1;
            }
            if (Input.GetKey(KeyCode.D))
            {
                dir.x = 1;
                _sprite.flipX = false;
            }

            if (Input.anyKey)
            {
                _animator.Play("WALK");
            }
            else
            {
                _animator.Play("IDLE");
            }

            dir.Normalize();

            GetComponent<Rigidbody2D>().velocity = _speed * dir;



            if (Input.GetKeyDown(KeyCode.I))
            {
                if (_inventoryMenu.isActiveAndEnabled == false)
                {
                    _inventoryMenu.Show();
                    foreach (var item in GameEngine.Instance.GetInventoryController().GetInventoryModel().GetCurrentInventoryState())
                    {
                        _inventoryMenu.UpdateData(item.Key, item.Value.item.GetIcon(), item.Value.quantity);
                    }
                }
                else
                {
                    _inventoryMenu.Hide();
                }
            }
        }
        #endregion
    }
}
using TMPro;
using UnityEngine;

namespace Assets.View.Common.Scripts
{
    public class BattleUI : MonoBehaviour
    {

        [SerializeField] private TMP_Text _dialogText;
        [SerializeField] private TMP_Text _infoText;

        #region ENABLE & DISABLE
        public void DisableDialogText()
        {
            _dialogText.gameObject.SetActive(false);
        }
        public void EnableDialogText()
        {
            _dialogText.gameObject.SetActive(true);
        }
        public void DisableInfoText()
        {
            _infoText.gameObject.SetActive(false);
        }
        public void EnableInfoText()
        {
            _infoText.gameObject.SetActive(true);
        }
        #endregion

        #region GETTERS
        public void SetDialogText(string text)
        {
            EnableDialogText();
            _dialogText.text = text;
        }
        #endregion

        #region SETTERS
        public void SetInfoText(string text)
        {
            EnableInfoText();
            _infoText.text = text;
        }
        #endregion

    }
}

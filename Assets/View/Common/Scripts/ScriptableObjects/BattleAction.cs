﻿using Assets.View.Common.Scripts.Enum;
using Assets.View.Common.Scripts.Model.Fighter;
using UnityEngine;

namespace Assets.View.Common.Scripts.ScriptableObjects
{
    public abstract class BattleAction:ScriptableObject
    {

        [SerializeField] private int _amount;

        [SerializeField] private int _range;

        [SerializeField] private int _actionCost;

        [SerializeField] private BattleActionEnum _bEnum;

        #region Getters
        public int GetAmount() => _amount;
        public int GetRange() => _range;
        public int GetActionCost() => _actionCost;
        public BattleActionEnum GetBattleActionEnum() => _bEnum;
        #endregion

        #region Setters
        public void SetAmount(int pAmount) => _amount = pAmount;
        public void SetRange(int pRange) => _range = pRange;
        public void SetActionCost(int pActionCost) => _actionCost = pActionCost;
        #endregion

    }
}
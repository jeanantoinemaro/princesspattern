﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Assets.View.Common.Scripts.Controller.UI;
using Assets.View.Common.Scripts.Model.State;
using UnityEngine;
using UnityEngine.Tilemaps;
using UnityEngine.UI;

namespace Assets.View.Common.Scripts.Model.Map
{
    public class TileGo: MonoBehaviour
    {
        [SerializeField] private GameObject _highlight;

        [SerializeField]
        private GameObject _highlight2;

        public int G;
        public int H;
        public int F { get { return G + H; } }

        public bool isBlocked = false;
        public float speed;
        public TileGo Previous;
        private List<TileGo> path;
        public Vector3Int gridLocation;

        private bool _selected;

        public void OnMouseEnter()
        {
            Highlight();
            _highlight.GetComponent<SpriteRenderer>().sortingOrder = this.GetComponent<SpriteRenderer>().sortingOrder;

        }

        public void OnMouseDown()
        {
            StartCoroutine(BattleEngine.Instance.GetState().DispatchPlayer(this));
            StartCoroutine(BattleEngine.Instance.GetState().ChooseTile(this));

        }
        public void OnMouseExit()
        {
            DeactivateHighlight();
        }

        //public GameObject GetHighlight()
        //{
        //    return _highlight;

        //}

        //public GameObject GetHighlight2()
        //{
        //    return _highlight2;
        //}

        public void Highlight()
        {
            if (!isBlocked)
            {
                _highlight.SetActive(true);
            }
        }

        public void Highlight2()
        {
            if (!isBlocked)
            {
                _highlight2.SetActive(true);
            }
        }

        public void DeactivateHighlight()
        {
            _highlight.SetActive(false);
        }

        public void DeactivateHighlight2()
        {
            _highlight2.SetActive(false);
        }

        public void SetSelected(bool pSelected)
        {
            _selected = pSelected;
        }

    }
}

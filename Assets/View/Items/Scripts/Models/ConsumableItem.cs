using UnityEngine;
using Assets.View.Items.Scripts.Enums;
using System;
using Assets.View.Items.Scripts.Models.StatModifiers;
using Assets.View.Items.Scripts.Interfaces;
using Assets.View.Common.Scripts.Model.Fighter.Player;

namespace Assets.View.Items.Scripts.Models
{
    [CreateAssetMenu(menuName = "Consumable", order = 3)]
    public class ConsumableItem : Item, IConsumable
    {
        #region Public Methods

        public ConsumableItem()
        {
            SetType(EItemType.Consumable);
            SetMaxStackSize(99);
        }

        public bool ConsumeAffectCharacter(PlayerFighter pPlayer)
        {
            foreach (ModifierData data in GetModifiers())
            {
                data.StatModifier.ConsumableAffectCharacter(pPlayer, data.value);
            }
            return true;
        }

        #endregion
    }

    [Serializable]
    public class ModifierData
    {
        #region Fields

        public CharacterStatModifier StatModifier;
        public float value;

        #endregion
    }
}

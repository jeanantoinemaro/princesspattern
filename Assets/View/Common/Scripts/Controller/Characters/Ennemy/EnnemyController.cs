﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Assets.View.Common.Scripts.Template.Ennemy;
using Assets.View.Common.Scripts.Template.Character;
using UnityEngine;

namespace Assets.View.Common.Scripts.Controller.Characters.Ennemy
{
    public class EnnemyController:FighterController
    {
        #region Private fields

        #endregion
        public EnnemyController(Model.Fighter.Ennemy.Ennemy enemy, FighterTemplate template):base(enemy, template)
        {
           
        }

    }
}

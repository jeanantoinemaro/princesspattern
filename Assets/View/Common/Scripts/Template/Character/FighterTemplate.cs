﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Assets.View.Common.Scripts.Controller.Characters;
using Assets.View.Common.Scripts.Controller.UI;
using Assets.View.Common.Scripts.Enum;
using Assets.View.Common.Scripts.Model.Map;
using UnityEngine;

namespace Assets.View.Common.Scripts.Template.Character
{
    public class FighterTemplate : MonoBehaviour
    {
        #region Serialized Fields
        [SerializeField] private SpriteRenderer _sprite;
        [SerializeField] private Animator _animator;
        [SerializeField] private PlayerCharacterEnum _cEnum;
        [SerializeField] private string _name;
        [SerializeField] private int _life;
        [SerializeField] private int _maxLife;
        [SerializeField] private HealthBar _healthBar;
        #endregion
        public event Action Selected;

        public event Action Targeted;

        #region Unity Methods

        public void Awake()
        {
            _animator = GetComponent<Animator>();
        }

        public void Start()
        {
            _healthBar.SetHealth(_life, _maxLife);
        }

        #endregion
        #region Getters Setters

        public string GetName()
        {
            return _name;
        }
        public void SetName(string pName)
        {
            _name = pName;
        }
        public PlayerCharacterEnum GetPlayerCharacterEnum()
        {
            return _cEnum;
        }
        public void SetPlayerCharacterEnum(PlayerCharacterEnum pEnum)
        {
            _cEnum = pEnum;
        }

        public SpriteRenderer GetSprite()
        {
            return _sprite;
        }

        public void SetSprite(Sprite sprite)
        {
            _sprite.sprite = sprite;
        }

        public void SetMaxLife(int pLife)
        {
            _maxLife = pLife;
        }
        #endregion

        #region Public methods

        public void SetLife(int pLife)
        {
            _life = pLife;
            _healthBar.SetHealth(_life, _maxLife);
        }
        public void OnSelected()
        {
            Selected?.Invoke();
        }

        public void OnTargeted()
        {
            Targeted?.Invoke();
        }

        public void PlayAnimator(String state)
        {
            _animator.Play(state);
        }

        public void Die()
        {
            PlayAnimator("DIE");
            Destroy(this.gameObject, this.GetComponent<Animator>().GetCurrentAnimatorStateInfo(0).length);
        }
        #endregion
    }
}

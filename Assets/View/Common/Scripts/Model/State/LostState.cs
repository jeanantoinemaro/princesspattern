using System.Collections;
using System.Collections.Generic;
using Assets.View.Common.Scripts.Model.Util;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace Assets.View.Common.Scripts.Model.State
{
    public class LostState : State
    {
        public override IEnumerator Start()
        {
            BattleEngine.Instance.BattleUI.SetDialogText("Defeat");
            yield return new WaitForSeconds(2f);
            BattleEngine.Instance.BattleUI.DisableDialogText();
            SceneManager.LoadScene("GameOver");
            SaveFighters.DeleteAllFighters();
            SaveWorldPlayer.DeleteSave();
            yield break;
        }
    }
}
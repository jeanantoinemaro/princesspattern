﻿using UnityEngine;
using System;

namespace Assets.View.Common.Scripts.ScriptableObjects
{
    [Serializable]
    public class Character: ScriptableObject
    {
        #region Fields

        [Header("Character Settings")]
        [SerializeField] private int _baseLife;
        private int _maxLife;
        private int _life;

        [SerializeField] private int _baseInitiative;
        private int _initiative;

        //private bool _isInDanger;

        public event Action OnDamage;
        public event Action<int> OnLifeChange;
        public event Action OnDeath;
        public event Action<int> OnInit;

        //[SerializeField]
        //private int _level;

        //[SerializeField]
        //private int _experience;

        //[SerializeField]
        //private int _maxExperience;

        #endregion

        #region Getters
        public int GetBaseLife() => _baseLife;
        public int GetLife() => _life;
        public int GetMaxLife() => _maxLife;
        //public bool GetIsInDanger() => _isInDanger;
        public int GetBaseInitiative() => _baseInitiative;
        public int GetInitiative() => _initiative;
        //public int GetLevel() => _level;
        //public int GetExperience() => _experience;
        //public int GetMaxExperience() => _maxExperience;
        #endregion

        #region Setters
        public void SetLife(int pLife) => _life = pLife;
        public void SetMaxLife(int pMaxLife) => _maxLife = pMaxLife;
        //public void SetIsInDanger(bool pIsInDanger) => _isInDanger = pIsInDanger;
        public void SetInitiative(int pInitiative) => _initiative = pInitiative;
        //public void SetLevel(int pLevel) => _level = pLevel;
        //public void SetExperience(int pExperience) => _experience = pExperience;
        //public void SetMaxExperience(int pMaxExperience) => _maxExperience = pMaxExperience;
        #endregion

        public void Damage(int pDamage)
        {
            _life = Math.Max(_life - pDamage, 0);
            OnLifeChange?.Invoke(_life);


            if (IsDead())
            {
                OnDeath?.Invoke();
            } else
            {
                OnDamage?.Invoke();
            }
        }

        public void Heal(int pLife)
        {
            _life = Math.Min(_life + pLife, _maxLife);
            OnLifeChange?.Invoke(_life);
        }

        public bool IsDead()
        {
            if (_life > 0)
                return false;
            else
                return true;
        }

        public void Die()
        {
            _life = 0;
            //SceneManager.LoadScene("???");
        }

        public void ResetCharacter()
        {
            if (_maxLife == 0) _maxLife = _baseLife;
            _life = _maxLife;
            OnLifeChange?.Invoke(_life);
            OnInit?.Invoke(_maxLife);
        }

        public void UpgradeLife()
        {
            _baseLife += 10;
            _maxLife += 10;
            ResetCharacter();
        }

        public void OnEnable()
        {
            ResetCharacter();
        } 

        public override string ToString()
        {
            return "Character : " +
                "_baseLife : " + _baseLife.ToString() + "; " +
                "_life : " + _life.ToString() + "; " +
                "_maxLife : " + _maxLife.ToString() + "; " 
                //+ "_isInDanger : " + _isInDanger.ToString() 
                ;
        }
    }
}

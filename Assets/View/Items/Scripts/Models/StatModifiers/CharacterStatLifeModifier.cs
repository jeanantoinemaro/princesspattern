using Assets.View.Common.Scripts.Model.Fighter.Player;
using Assets.View.Common.Scripts.ScriptableObjects.AdvancedCharacter;
using UnityEngine;

namespace Assets.View.Items.Scripts.Models.StatModifiers
{
    [CreateAssetMenu(menuName = "Stat Modifier/Life Modifier", order = 0)]
    public class CharacterStatLifeModifier : CharacterStatModifier
    {
        public override void ConsumableAffectCharacter(PlayerFighter pPlayer, float pValue)
        {
            AdvancedCharacter advancedCharacter = pPlayer.GetCharacter();
            if (advancedCharacter != null)
            {
                advancedCharacter.Heal((int)pValue);
            }
        }

        public override void EquipableAffectCharacter(PlayerFighter pPlayer, float pValue)
        {
            AdvancedCharacter advancedCharacter = pPlayer.GetCharacter();
            if (advancedCharacter != null)
            {
                advancedCharacter.SetLife(advancedCharacter.GetLife() + (int)pValue);
                advancedCharacter.SetMaxLife(advancedCharacter.GetMaxLife() + (int)pValue);
            }
        }
    }
}
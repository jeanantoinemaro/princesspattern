namespace Assets.View.Items.Scripts.Enums
{
    public enum EEquipType
    {
        Helmet,
        Torso,
        Gloves,
        Weapon
    }
}


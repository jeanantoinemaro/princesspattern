﻿using UnityEngine;

namespace Assets.View.Common.Scripts.ScriptableObjects.Moves
{
    [CreateAssetMenu(fileName = "New Action", menuName = "Action/Heal")]
    public class HealAction : BattleAction
    {
        private int _regenHeal;

        [SerializeField]
        private SpriteRenderer _sprite;

        public int GetRegenHeal() => _regenHeal;

        public void SetRegenHeal(int pRegenHeal) => _regenHeal = pRegenHeal;

        public void CalculateHeal()
        {
            _regenHeal = GetAmount();
        }
        public void RegenHeal()
        {
            CalculateHeal();
            // GetAdvancedCharacter().Heal(_regenHeal);
        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Assets.View.Common.Scripts.Controller.Characters.Ennemy;
using Assets.View.Common.Scripts.Controller.Characters.Players;
using Assets.View.Common.Scripts.Model.Fighter;
using Assets.View.Common.Scripts.Model.Map;
using Assets.View.Common.Scripts.Template.Character;
using UnityEngine;
using Random = UnityEngine.Random;

namespace Assets.View.Common.Scripts.Controller.Characters
{
    public class FighterController
    {
        [SerializeField] protected FighterTemplate _fighterTemplate;
        protected bool _isReady;
        protected bool _isDead;
        protected Fighter _fighter;
        public string _name;
        public event Action<FighterController> Selected;

        public FighterController(Fighter fighter, FighterTemplate template)
        {
            _name = Random.Range(0, 100).ToString();
            _fighter = fighter;
            _fighterTemplate = template;
            _fighterTemplate.SetMaxLife(_fighter.GetCharacter().GetMaxLife());
            _fighterTemplate.SetLife(_fighter.GetCharacter().GetLife());
            _fighter.GetCharacter().OnLifeChange += OnLifeChange;
            _fighter.GetCharacter().OnDamage += OnDamage;
            _fighter.GetCharacter().OnDeath += OnDeath;
            _fighter.GetCharacter().OnDeath += _fighterTemplate.Die;
        }

        public void OnEnable()
        {

        }
        public void OnSelected()
        {
            Selected?.Invoke(this);
        }

        public void OnLifeChange(int pLife)
        {
            _fighterTemplate.SetLife(pLife);
        }

        public void OnDeath()
        {
            _isDead = true;
            GetStandingOnTile().isBlocked = false;
            if (this is PlayerController)
            {
                BattleEngine.Instance.AreAllPlayersDead();
            }
            else
            {
                BattleEngine.Instance.AreAllEnemiesDead();
            }
        }

        public void OnDamage()
        {
            _fighterTemplate.PlayAnimator("HURT");
        }

        public void Defend()
        {
            _fighter.SetIsDefending(true);
        }

        #region Getters Setters
        public bool IsReady()
        {
            if (_isDead) return false;
            return _isReady;
        }

        public void SetIsReady(bool pIsReady)
        {
            _isReady = pIsReady;
        }

        public bool IsDead()
        {
            return _isDead;
        }

        public FighterTemplate GetFighterTemplate()
        {
            return _fighterTemplate;
        }

        public void SetFighterTemplate(FighterTemplate pTemplate)
        {
            _fighterTemplate = pTemplate;
        }

        public Fighter GetFighter()
        {
            return _fighter;
        }

        public void SetFighter(Fighter pFighter)
        {
            _fighter = pFighter;
        }

        public void SetStandingOnTile(TileGo tile)
        {
            _fighter.SetStandingOnTile(tile);
        }

        public TileGo GetStandingOnTile()
        {
            return _fighter.GetStandingOnTile();
        }

        public void SetDestinationTile(TileGo tile)
        {
            _fighter.SetDestinationTile(tile);
        }
        #endregion


        #region Public methods

        public bool CanMove()
        {
            return _fighter.GetCharacter().GetMovementPoint() > 0;
        }
        public void DecrementMovementPoints()
        {
            _fighter.GetCharacter().DecrementMovementPoints();
        }
        public void ResetMovementPoints()
        {
            _fighter.GetCharacter().ResetMovementPoints();
        }
        #endregion

    }
}
using Assets.View.Common.Scripts.Model.Player;
using Assets.View.Common.Scripts.Model.Util;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace Assets.View.Common.Scripts
{
    public class BattleLauncher : MonoBehaviour
    {
        [SerializeField]
        private string _battleScene;

        private void OnCollisionEnter2D(Collision2D collision)
        {
            SaveWorldPlayer.Save(collision.gameObject.GetComponent<Player>());
            SaveInventory.Save(GameEngine.Instance.GetInventoryController().GetInventoryModel().GetCurrentInventoryState());
            WorldEngine.Instance.SetCurrentEnnemy(gameObject);
            SceneManager.LoadScene(_battleScene);
        }
    }
}

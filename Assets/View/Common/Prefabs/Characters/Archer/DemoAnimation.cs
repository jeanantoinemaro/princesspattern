using UnityEngine;

namespace Assets.View.Common.Prefabs.Characters.Archer
{
    public class DemoAnimation : MonoBehaviour
    {
        Animator animator;
        void Start()
        {
            animator = GetComponent<Animator>();
            animator.Play("IDLE");
        }

        // Update is called once per frame
        void Update()
        {
            if (Input.GetKeyDown(KeyCode.Space))
                animator.Play("IDLE");
            else if (Input.GetKeyDown(KeyCode.A))
                animator.Play("WALK");
            else if (Input.GetKeyDown(KeyCode.Z))
                animator.Play("SHOOT");
            else if (Input.GetKeyDown(KeyCode.E))
                animator.Play("HURT");
            else if (Input.GetKeyDown(KeyCode.R))
                animator.Play("HEAL");
            else if (Input.GetKeyDown(KeyCode.T))
                animator.Play("DIE");

        }
    }
}

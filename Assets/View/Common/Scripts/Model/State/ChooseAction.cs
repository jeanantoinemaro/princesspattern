﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

namespace Assets.View.Common.Scripts.Model.State
{
    public class ChooseAction : State
    {

        public override IEnumerator Start()
        {
            BattleEngine.Instance.GetCurrentPlayerController().GetFighterTemplate().PlayAnimator("IDLE");
            BattleEngine.Instance.GetActions().gameObject.SetActive(true);
            yield break;
        }

        public override IEnumerator AttackChoice()
        {
            BattleEngine.Instance.SetState(new AttackState());
            yield break;
        }

    }
}

using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Assets.View.Common.Scripts.Model.Map;
using UnityEngine;

namespace Assets.View.Common.Scripts.Model.State
{
    public class ChooseDestination : State
    {

        public override IEnumerator Start()
        {
            BattleEngine.Instance.BattleUI.SetInfoText("Choose a tile");
            yield break;
        }

        public override IEnumerator ChooseTile(TileGo tile)
        {
            Fighter.Player.PlayerFighter currentPlayer = (Fighter.Player.PlayerFighter) BattleEngine.Instance.GetCurrentPlayerController().GetFighter();
            currentPlayer.SetDestinationTile(tile);
            MapEngine.Instance.SetTilesTo(false, currentPlayer.GetCharacter().GetMovementPoint());
            BattleEngine.Instance.BattleUI.DisableInfoText();
            BattleEngine.Instance.SetState(new Move());
            yield break;
        }
    }
}

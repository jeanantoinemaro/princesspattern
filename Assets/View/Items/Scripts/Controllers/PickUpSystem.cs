using Assets.View.Items.Scripts.Models;
using UnityEngine;

namespace Assets.View.Items.Scripts.Controllers
{
    public class PickUpSystem : MonoBehaviour
    {
        #region Serialized Fields

        [SerializeField] private InventoryModel _inventoryData;

        #endregion

        #region Setters

        public void SetInventory(InventoryModel pInventory)
            => _inventoryData = pInventory;

        #endregion

        #region Unity Methods

        private void OnTriggerEnter2D(Collider2D collision)
        {
            PickableItem item = collision.GetComponent<PickableItem>();
            if (item != null)
            {
                int reminder = _inventoryData.AddItem(item.GetItem(), item.GetQuantity());
                if (reminder == 0)
                    item.DestroyItem();
                else
                    item.SetQuantity(reminder);
            }
        }

        #endregion
    }
}
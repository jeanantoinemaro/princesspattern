﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Assets.View.Common.Scripts.Controller.Characters;
using Assets.View.Common.Scripts.Enum;
using UnityEngine;
using Assets.View.Common.Scripts.Model.Map;
using Action = Assets.View.Common.Scripts.ScriptableObjects.BattleAction;

namespace Assets.View.Common.Scripts.Model.State
{
    public class AttackState:State
    {

        private FighterController _target;
        public override IEnumerator Start()
        {
            if (BattleEngine.Instance.GetCurrentPlayerController().GetFighter().IsDefending())
            {
                BattleEngine.Instance.BattleUI.SetInfoText("Defending");
            } else
            {
                BattleEngine.Instance.BattleUI.SetInfoText("Choose a target");
            }
                
            yield break;
        }


        public override IEnumerator TargetChoice()
        {
            if (!BattleEngine.Instance.GetCurrentPlayerController().GetFighter().IsDefending())
            {
                BattleActionEnum battleActionType = BattleEngine.Instance.GetCurrentPlayerController().GetFighter()
                .GetCharacter().GetCurrentMove().GetBattleActionEnum();

                int distance = IsTargetInRange();
                if (0 < distance)
                {
                    BattleEngine.Instance.BattleUI.SetDialogText("Target is too far by " + distance + " tiles.");
                    yield return new WaitForSeconds(1.5f);
                    BattleEngine.Instance.BattleUI.DisableDialogText();
                    yield break;
                }


                if (battleActionType == BattleActionEnum.Attack)
                {
                    BattleEngine.Instance.GetCurrentPlayerController().GetFighter().GetCharacter().Attack(_target.GetFighter());
                    BattleEngine.Instance.GetCurrentPlayerController().GetFighterTemplate().PlayAnimator("SHOOT");

                }
                else if (battleActionType == BattleActionEnum.Heal)
                {
                    BattleEngine.Instance.GetCurrentPlayerController().GetFighter().GetCharacter().Heal(_target.GetFighter());
                    BattleEngine.Instance.GetCurrentPlayerController().GetFighterTemplate().PlayAnimator("SHOOT");
                    _target.GetFighterTemplate().PlayAnimator("HEAL");
                }
            }
            
            BattleEngine.Instance.GetCurrentPlayerController().SetIsReady(false);
            BattleEngine.Instance.BattleUI.DisableInfoText();
            BattleEngine.Instance.SetState(new SelectNextPlayer());
            
            yield break;
        }

        public override IEnumerator SelectController(FighterController fController)
        {
            _target = fController;
            yield break;
           
        }

        private int IsTargetInRange()
        {
            FighterController currentController = BattleEngine.Instance.GetCurrentPlayerController();
            int distance = new PathFinder().FindStraightDistance(currentController.GetStandingOnTile(), _target.GetStandingOnTile());
           return distance - currentController.GetFighter().GetCharacter().GetCurrentMove().GetRange();
        }

        public override IEnumerator CancelAttack()
        {
            int currentAttackRange = BattleEngine.Instance.GetCurrentPlayerController().GetFighter().GetCharacter()
                .GetCurrentMove().GetRange();
            MapEngine.Instance.SetTilesTo(false,currentAttackRange);
            BattleEngine.Instance.SetState(new ChooseAction());
            yield break;
        }
    }
}

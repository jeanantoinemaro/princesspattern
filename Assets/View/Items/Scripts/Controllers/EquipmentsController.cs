using System.Collections.Generic;
using Assets.View.Common.Scripts.Model.Fighter.Player;
using Assets.View.Items.Scripts.Enums;
using Assets.View.Items.Scripts.Interfaces;
using Assets.View.Items.Scripts.Models;
using Assets.View.Items.Scripts.UI;
using UnityEngine;

namespace Assets.View.Items.Scripts.Controllers
{
    /// <summary>
    /// Class which keeps track of the character's current equipment
    /// </summary>
    public class EquipmentsController // : MonoBehaviour
    {
        #region Private Fields

        private UI_EquipmentsPanelTitle _playerMenuTitle;
        private UI_StatsMenu _statsMenu;
        private UI_EquipmentsMenu _equipmentsMenu;
        private EquipmentsModel _equipmentsData;
        private List<EquipmentItem> _initialItems;
        private InventoryController _inventoryController;
        private PlayerFighter _player;

        #endregion

        #region Getters

        public EquipmentsModel GetEquipmentsModel()
            => _equipmentsData;

        #endregion

        #region Constructors

        public EquipmentsController(PlayerFighter pMainPlayer, Object pEquipmentsPanel, InventoryController pInventoryController)
        {
            _player = pMainPlayer;
            _playerMenuTitle = (pEquipmentsPanel as GameObject).GetComponentInChildren<UI_EquipmentsPanelTitle>();
            _statsMenu = (pEquipmentsPanel as GameObject).GetComponentInChildren<UI_StatsMenu>();
            _equipmentsMenu = (pEquipmentsPanel as GameObject).GetComponentInChildren<UI_EquipmentsMenu>();
            _equipmentsData = ScriptableObject.CreateInstance<EquipmentsModel>();
            _initialItems = new List<EquipmentItem>();
            _inventoryController = pInventoryController;

            PrepareUI();
            PrepareEquipmentsData();
        }

        #endregion

        #region Private Methods

        private void PrepareUI()
        {
            _playerMenuTitle.SetTitle(_player.GetName());
            _statsMenu.InitializeStatsUI(_player.GetCharacter());
            _equipmentsMenu.InitializeEquipmentsUI(_equipmentsData.GetSize());
            _equipmentsMenu.OnDetailsRequested += HandleDetailsRequested;
            _equipmentsMenu.OnItemActionRequested += HandleItemActionRequest;
        }

        private void PrepareEquipmentsData()
        {
            _equipmentsData.Initialize();
            _equipmentsData.OnEquipmentsUpdated += UpdateEquipmentsUI;
            foreach (EquipmentItem item in _initialItems)
            {
                if (item.GetIcon() == null)
                {
                    continue;
                }
                _equipmentsData.AddEquipment(item);
            }
        }

        private void UpdateEquipmentsUI(Dictionary<EEquipType, EquipmentItem> _equipments)
        {
            _equipmentsMenu.ResetAllItems();
            foreach (var item in _equipments)
            {
                _equipmentsMenu.UpdateData((int)item.Key, item.Value.GetIcon());
            }
            UpdateStatsUI();
        }

        public void UpdateStatsUI()
        {
            _statsMenu.UpdateData(_player.GetCharacter());
        }

        private void HandleDetailsRequested(int pItemIndex) // feature not finished
        {
            EquipmentItem equipmentItem = _equipmentsData.GetEquipmentAt(pItemIndex);
            if (equipmentItem.GetIcon() == null)
            {
                //_equipmentMenu.ResetSelection();
                return;
            }
            Item item = equipmentItem;
            //_equipmentMenu.UpdateDetails(pItemIndex, item.GetIcon(), item.name, "");
        }

        private void HandleItemActionRequest(int pItemIndex)
        {
            EquipmentItem equipmentItem = _equipmentsData.GetEquipmentAt(pItemIndex);
            if (equipmentItem.GetIcon() == null)
                return;
            if (equipmentItem is IEquipable)
            {
                equipmentItem.UnequipAffectCharacter(_player);
                _inventoryController.GetInventoryModel().AddItem(_equipmentsData.GetEquipmentAt(pItemIndex), 1);
                _equipmentsData.RemoveEquipment(pItemIndex);
            }
        }

        #endregion
    }
}

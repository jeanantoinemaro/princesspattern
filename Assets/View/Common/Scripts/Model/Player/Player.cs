﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Assets.View.Common.Scripts.Model.Map;
using Assets.View.Common.Scripts.Model.State;
using Assets.View.Common.Scripts.ScriptableObjects;
using Assets.View.Common.Scripts.ScriptableObjects.AdvancedCharacter;
using Assets.View.Items.Scripts;
using Assets.View.Items.Scripts.Models;
using UnityEngine;

namespace Assets.View.Common.Scripts.Model.Player
{
    public class Player:MonoBehaviour
    {
        [Header("Character")]
        private int _level;
        [SerializeField]
        private string _name;
        [SerializeField]
        private AdvancedCharacter _character;
        private int _experiences;
        private int _maxExperiences;
        private TileGo _standingOnTile;
        private TileGo _destinationTile;
        private bool _selected;
        private bool _isReady;
        [SerializeField] private SpriteRenderer _sprite;

        //private Inventory _inventory;
        //[SerializeField] private UI_Inventory _uiInventory;
        //private EquipmentEngine _equipmentEngine;

        public void AddExperiences(int pExperience)
        {
            _experiences += pExperience;
        }

        private bool IsReadyToLevelUp() => _experiences == _maxExperiences;
        private void LevelUp()
        {
            _level += 1;
            _experiences -= _maxExperiences;
            _maxExperiences *= 2;
        }

        #region GETTERS
        public TileGo GetStandingOnTile()
        {
            return _standingOnTile;
        }
        public TileGo GetDestinationTile()
        {
            return _destinationTile;
        }
        public bool GetSelected()
        {
            return _selected;
        }

        public bool IsReady()
        {
            return _isReady;
        }
        public AdvancedCharacter GetCharacter()
        {
            return _character;
        }
        #endregion

        #region SETTERS
        public void SetStandingOnTile(TileGo tile)
        {
            _standingOnTile = tile;
        }

        public void SetDestinationTile(TileGo tile)
        {
            _destinationTile = tile;
        }

        public void SetSelected(bool pSelected)
        {
            _selected = pSelected;
        }

        private void UseConsummableItem(Item item)
        {
            //_character.SetLife(_character.GetBaseLife() + item.GetEffects()[0].amount);
        }

        //public void Update()
        //{

        //    StartCoroutine(BattleEngine.Instance.GetState().MovePlayer());
        //}

        //public AdvancedCharacter GetCharacter()
        //{
        //    //_inventory = Inventory.instance;
        //    //_inventory = gameObject.AddComponent<Inventory>();
        //    //_uiInventory.SetInventory(_inventory);
        //    //_equipmentEngine = gameObject.AddComponent<EquipmentEngine>();
        //    return _character;
        //}
        
        public string GetName() => _name;

        #endregion
    }
}
